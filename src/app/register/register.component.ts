import { Component, OnInit } from '@angular/core';
import { ToastModule } from 'primeng/toast';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { MessageService } from 'primeng/api';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { formValidators } from '../helper/formValidators';
import { ClientAdministrationService } from '../services/client-administration.service';
declare var $: any
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  providers: [ToastModule,MessageService],
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  clientAdministrationForm: FormGroup;
  selectedValue: any;
  clientAdministration: any;
  showCount: boolean;
  disabled:boolean;
  uploadData: any;
  webUrl: string;
  organizationName: string;
  pointOfContact: string;
  phoneNumber: string;
  firstName: string;
  lastName: string;
  address: string;
  password: string;
  emailId: string;
  userId: number;
  userLoginId: number;
  displayModal: boolean;
  successMessage: any;

  constructor(private router: Router, private http: HttpClient,private ClientAdministration: ClientAdministrationService, private formBuilder: FormBuilder,  private messageService: MessageService) { 
  }
  
  ngOnInit() {
    
    this.clientAdministrationForm = this.formBuilder.group({
      OrganizationName: ["", [Validators.required, formValidators.alphabetical, formValidators.noWhitespace, Validators.maxLength(50)]],
      WebUrl: ["", [Validators.required,formValidators.url]],
      Address: ["", [Validators.required, formValidators.address]],
      FirstName: ["", [Validators.required, formValidators.alphabetical, formValidators.noWhitespace, Validators.maxLength(50)]],
      LastName: ["", [Validators.required, formValidators.alphabetical, formValidators.noWhitespace, Validators.maxLength(50)]],
      Email: ["", [Validators.required, formValidators.email]],
      Password: ["", [Validators.required, formValidators.noWhitespace,  Validators.minLength(8), Validators.maxLength(8)]],
      PhoneNumber: ["", [Validators.required, formValidators.noWhitespace, formValidators.numeric, Validators.minLength(7), Validators.maxLength(15)]],
      active: "",
      rememberMeFlag: [false]
    });
  }

  public get getFields() {
    return this.clientAdministrationForm.controls;
  }


   validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  submitForm(formData) {
    debugger
    this.validateAllFormFields(this.clientAdministrationForm);

    if (this.clientAdministrationForm.valid) {
      this.disabled = true;
      var data = {
        OrganizationName: formData["OrganizationName"],
        WebUrl: formData["WebUrl"],
        Address: formData["Address"],
        FirstName: formData["FirstName"],
        LastName: formData["LastName"],
        PhoneNumber: formData["PhoneNumber"],
        EmailId: formData["Email"],
        Password: formData["Password"],
        Active: (formData["active"] == "true"),
        RecordStatus: 1,
        InsertedBy: 1,
      }

      this.ClientAdministration.postRequest('InsertClientDetails', data).subscribe((response) => {
        if (response.status == 1) {
          $('.close').trigger('click');
          this.Close();          
          this.successmessage("Client created successfully");
          this.disabled = false;
          //this.showModalDialog("");
          this.router.navigate(['track']);
        }
        else {
          this.Errormessage(response["message"]);
          this.disabled = false;
        }
      });
    }
  }

  showModalDialog(message: any) {
    this.displayModal = true;
    this.successMessage = message;
}

// Navigate(){
//   this.router.navigate(['login']);
// }

 numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }

  Close() {
    this.clientAdministrationForm.reset();
  }

  successmessage(message) {
    this.messageService.add({severity:'success', summary:'Success', detail:message});
 }

 Errormessage(errorsmessage) {
   this.messageService.add({ severity: 'error', summary: 'Error', detail: errorsmessage });
 }


}
