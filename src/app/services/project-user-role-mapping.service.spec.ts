import { TestBed } from '@angular/core/testing';

import { ProjectUserRoleMappingService } from './project-user-role-mapping.service';

describe('ProjectUserRoleMappingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProjectUserRoleMappingService = TestBed.get(ProjectUserRoleMappingService);
    expect(service).toBeTruthy();
  });
});
