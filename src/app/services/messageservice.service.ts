import { Injectable } from '@angular/core';
import {Component} from '@angular/core';
import {Message} from 'primeng//api';
import {MessageService} from 'primeng/api';

@Injectable()
export class MessageserviceService {

 constructor(private messageService: MessageService) {}


 successmesage(message){
  this.messageService.add({severity:'success', summary:'', detail:message});
}

addMultiple() {
  this.messageService.addAll([{severity:'success', summary:'Service Message', detail:'Via MessageService'},
                  {severity:'info', summary:'Info Message', detail:'Via MessageService'}]);
}
}
