import { TestBed } from '@angular/core/testing';

import { ClientAdminUserBoardService } from './client-admin-user-board.service';

describe('ClientAdminUserBoardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientAdminUserBoardService = TestBed.get(ClientAdminUserBoardService);
    expect(service).toBeTruthy();
  });
});
