import { TestBed } from '@angular/core/testing';

import { SharedvalueService } from './sharedvalue.service';

describe('SharedvalueService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SharedvalueService = TestBed.get(SharedvalueService);
    expect(service).toBeTruthy();
  });
});
