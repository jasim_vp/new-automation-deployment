import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Observable, BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SharedvalueService {
  public isFullScreen = true;
  private showSideBarBS = new BehaviorSubject(null);
  private currentPage = new BehaviorSubject(null);
  constructor(@Inject(DOCUMENT) private document: any) { }

  setSideBarDetails(val){
    this.showSideBarBS.next(val);
  }
  setCurrentPage(val){
    this.currentPage.next(val);
  }
  getCurrentPageDetails(){
    return this.currentPage.asObservable();
  }
  getSideBarDetail(){
    return this.showSideBarBS.asObservable();
  }

  fs() {
    const fullscreen: any = document.getElementById('fullscreen');
    if (this.isFullScreen) {

      this.isFullScreen = false;
      if (fullscreen.requestFullscreen) {
        fullscreen.requestFullscreen();
      } else if (fullscreen.mozRequestFullScreen) { /* Firefox */
        fullscreen.mozRequestFullScreen();
      } else if (fullscreen.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
        fullscreen.webkitRequestFullscreen();
      } else if (fullscreen.msRequestFullscreen) { /* IE/Edge */
        fullscreen.msRequestFullscreen();
      }
    } else {

      this.isFullScreen = true;

      if (this.document.exitFullscreen) {
        console.log("NoisFullscreen");
        this.document.exitFullscreen();
      } else if (this.document.mozCancelFullScreen) {
        this.document.mozCancelFullScreen();
      } else if (this.document.webkitExitFullscreen) {
        this.document.webkitExitFullscreen();
      } else if (this.document.msExitFullscreen) {
        this.document.msExitFullscreen();
      } else {

      }
    }
  }
}
