import { TestBed } from '@angular/core/testing';

import { RolelistserviceService } from './rolelistservice.service';

describe('RolelistserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RolelistserviceService = TestBed.get(RolelistserviceService);
    expect(service).toBeTruthy();
  });
});
