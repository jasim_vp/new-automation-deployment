import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class InputService {

  environment: any = "";
  constructor(private http: HttpClient) {
    this.environment = environment.projectConfigurationServiceUrl;
  }

  getRequest(route) {
    return this.http.get<any>(this.environment + route).pipe(map(res => res));
  }

  postRequest(route, data) {
    return this.http.post<any>(this.environment + route, data).pipe(map(res => res));
  }

  getAttribute(): Observable<any> {
    return this.http.get<any>('./assets/json/input-attriubute.json');
  }
  getDefaultAttribute(): Observable<any> {
    return this.http.get<any>('./assets/json/defaultAttribute.json');
  }

  getgroups(): Observable<any> {
    return this.http.get<any>('./assets/json/groups.json');
  }
}
