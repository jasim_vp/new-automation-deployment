import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { formValidators } from "../../helper/formValidators";
import { NgForm } from '@angular/forms';
import { UserManagementService } from '../../services/user-management.service';
import { ClientAdministrationService } from '../../services/client-administration.service';
import { SharedvalueService } from 'src/app/services/sharedvalue.service';
import { ExcelService } from '../../services/excel.service';
declare var $: any
@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  providers: [CommonService, MessageService, ToastModule, ExcelService],
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements OnInit {
  selectedValue: any;
  cols: any[];
  clients: any = [];
  userManagement: any;
  userManagementForm: FormGroup;
  BulkUploadDetailsForm: FormGroup;
  showCount: boolean;
  disabled: boolean;
  uploadData: any;
  firstName: string;
  lastName: string;
  phone: string;
  workPhone: string;
  emailId: string;
  selectedOrganization: any;
  OrganizationName: string;
  showSideBar = true;
  currentUserId: number;
  deleteUserName: string;
  public fileName = 'Choose File';
  status: any[];
  @ViewChild('myInput', { static: true })
  myInputVariable: any;
  error_response: any;
  formData: any;
  constructor(
    private ClientAdministration: ClientAdministrationService,
    private http: HttpClient, private formBuilder: FormBuilder,
    private UserManagement: UserManagementService,
    private messageService: MessageService,
    private excelService: ExcelService,
    public shrService: SharedvalueService
  ) { }

  ngOnInit() {
    $('.spinner').show();
    $('.modal').appendTo('#fullscreen');
    this.shrService.getSideBarDetail().subscribe(resp => { this.showSideBar = resp });

    this.selectedValue = 'true';
    this.GetUserManagementList();
    this.userManagementForm = this.formBuilder.group({
      OrganizationName: ["", [Validators.required, Validators.maxLength(50)]],
      FirstName: ["", [Validators.required, formValidators.alphabetical, formValidators.noWhitespace, Validators.maxLength(50)]],
      LastName: ["", [Validators.required, formValidators.alphabetical, formValidators.noWhitespace, Validators.maxLength(50)]],
      Email: ["", [Validators.required, formValidators.email]],
      Phone: ["", [Validators.required, formValidators.noWhitespace, formValidators.numeric,
      Validators.minLength(7), Validators.maxLength(15)]],
      WorkPhone: ["", [Validators.required, formValidators.noWhitespace, formValidators.numeric,
      Validators.minLength(7), Validators.maxLength(15)]],
      active: "",
      rememberMeFlag: [false]
    });

    this.BulkUploadDetailsForm = this.formBuilder.group({
      InputFile: ['', [Validators.required]]
    });


    this.cols = [
      { field: 'firstName', header: 'User Name' },
      { field: 'emailId', header: 'Email' },
      { field: 'phone', header: 'Phone' },
    ];

    this.GetAllClients();

  }


  handleFileInput(files: FileList) {
    if (files.length > 0) {
      this.fileName = files.item(0).name;
      const filetype = files.item(0).name.split('.');
      if (filetype[1] !== 'csv') {
        this.fileName = 'Choose File';
        this.Errormessage('Please upload csv format file');
      } else {
        this.BulkUploadDetailsForm.patchValue({ InputFile: { file: files.item(0) } });
      }
    } else {

    }
  }

  downloadCsv() {
    this.status = ['approved', 'rejected', 'pending'];
    const data = ['FirstName', 'LastName', 'Email', 'Phone', 'WorkPhone'];
    this.excelService.exportAsExcelFile(data, 'UserManagement_Details');
  }

  GetAllClients() {
    $('.spinner').show();
    this.ClientAdministration.getRequest('GetClientDetails').subscribe((ClientAdministrationDetails) => {
      for (let i = 0; i <= ClientAdministrationDetails.length - 1; i++) {
        this.clients.push({
          label: ClientAdministrationDetails[i].organizationName, value: ClientAdministrationDetails[i].clientId
        });
      }
      $('.spinner').hide();
      //this.clients = ClientAdministrationDetails;
    });
  }

  GetUserManagementList() {
    $('.spinner').show();
    this.UserManagement.getRequest('GetUserManagementList').subscribe((UserManagementDetails) => {
      console.log(UserManagementDetails);
      this.userManagement = UserManagementDetails;
      this.showCount = true;
      $('.spinner').hide();
    });
  }


  public get getFields() {
    return this.userManagementForm.controls;
  }
  chooseFile(e) {
    try {
      // localStorage.getItem('user_role_id');
      // localStorage.getItem('user_name');

      localStorage.getItem('user_id');
      this.fileName = e.target.files[0].name;
      this.formData = new FormData();
      this.formData.append('File', e.target.files[0]);
      // this.formData.append('filename',  e.target.files[0].name);
      this.formData.append('ClientId', localStorage.getItem('user_id'));
      this.formData.append('RecordStatus', 1);
      this.formData.append('InsertedBy', localStorage.getItem('user_id'));




    } catch (e) {
      console.log('not uploaded');
      this.fileName = 'Choose File';
    }

  }

  UploadFile() {
    //  formData need to send in post method
    // below
    $('.spinner').show();
    this.UserManagement.postRequest('Uploadfile', this.formData).subscribe(resp => {
      console.log(resp);
      if (resp.error !== "") {
        // error
        // this.Errormessage('Some of them are not created.');
        this.error_response = resp.error_json;
       
        $('#notInserted').modal('show');
        $('.spinner').hide();
      } else {
        //success
        $('.spinner').hide();
        this.successmessage('All users created successfully');
      }
    },error =>{
      
     this.Errormessage('Please try again later.');
     $('.spinner').hide();
    });


  }

  public get getField() {
    return this.BulkUploadDetailsForm.controls;
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  BulkInsertForm(formdata) {
    $('.spinner').show();
    this.validateAllFormFields(this.BulkUploadDetailsForm);
    if (this.BulkUploadDetailsForm.valid) {
      const formDatas = new FormData();
      formDatas.append('File', formdata.InputFile.file);
      formDatas.append('ClientId', localStorage.getItem('client_id'));
      formDatas.append('RecordStatus', '1');
      formDatas.append('InsertedBy', localStorage.getItem('user_id'));

      this.UserManagement.postRequest('Uploadfile', formDatas).subscribe((response) => {
        const result = response[0];
        if (result.error_json !== null) {
          this.error_response = result.error_json;
          $('#notInserted').modal('show');
          $('.spinner').hide();
        } else {
          $('.close').trigger('click');
          this.Close();
          $('.spinner').hide();
          this.successmessage('All users created successfully');
        }
      },err =>{
        $('.spinner').hide();
        this.Errormessage(err.message);
      });
    }
  }

  exportError() {
    import('xlsx').then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(this.error_response);
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'csv', type: 'array' });

      this.saveAsCSVFile(excelBuffer, 'UDP_User_management_error');
    });
  }

  saveAsCSVFile(buffer: any, fileName: string): void {
    import('file-saver').then(FileSaver => {


      const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      const EXTENSION_TYPE = '.csv';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + new Date().getTime() + EXTENSION_TYPE);
    });
  }

  submitForm(formData) {
    this.validateAllFormFields(this.userManagementForm);
    console.log(formData);
    if (this.userManagementForm.valid) {
      this.disabled = true;

      var data = {
        ClientId: formData["OrganizationName"] * 1,
        FirstName: formData["FirstName"],
        LastName: formData["LastName"],
        EmailId: formData["Email"],
        Phone: formData["Phone"],
        WorkPhone: formData["WorkPhone"],
        Active: (formData["active"] == "true"),
        RecordStatus: 1,
        InsertedBy: 1,
      }

      console.log(data);

      this.UserManagement.postRequest('InsertUserManagement', data).subscribe((response) => {
        if (response.status == 1) {
          $('.close').trigger('click');
          this.Close();
          this.GetUserManagementList();
          this.successmessage("User created successfully");
          this.disabled = false;
        }
        else {
          this.Errormessage(response["message"]);
          this.disabled = false;
        }
      });
    }
  }


  Open() {
    this.selectedValue = 'true';
  }

  Close() {
    this.userManagementForm.reset();
    this.BulkUploadDetailsForm.reset();
    this.myInputVariable.nativeElement.value = '';
    this.fileName = 'Choose File';
  }

  successmessage(message) {
    this.messageService.add({ severity: 'success', summary: 'Success', detail: message });
  }

  Errormessage(errorsmessage) {
    this.messageService.add({ severity: 'error', summary: 'Error', detail: errorsmessage });
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }


  editUserManagement(userManagement: any) {
    $('.spinner').show();
    const b  = this.clients.findIndex(x=> x.value === userManagement.clientId);
  let  selectedOrg =  {}
    if(b !== -1){
     selectedOrg = this.clients[b];
    }
    this.selectedOrganization = selectedOrg;
    this.firstName = userManagement.firstName;
    this.lastName = userManagement.lastName;
    this.phone = userManagement.phone;
    this.emailId = userManagement.emailId;
    this.workPhone = userManagement.workPhone;
    this.selectedValue = String(userManagement.active);
    this.uploadData = userManagement;
    $('#editUserManagement').modal('show');
    $('.spinner').hide();
  }


  updateForm(formData) {
    this.validateAllFormFields(this.userManagementForm);
    if (this.userManagementForm.valid) {
      this.disabled = true;
      var data = {
        UserId: this.uploadData["userId"],
        ClientId: formData["OrganizationName"] * 1,
        FirstName: formData["FirstName"],
        LastName: formData["LastName"],
        EmailId: formData["Email"],
        Phone: formData["Phone"],
        WorkPhone: formData["WorkPhone"],
        InsertedBy: 1,
        UpdatedBy: 1,
        InsertedDateTime: this.uploadData["insertedDateTime"],
        Active: (formData["active"] == "true")
      }

      this.UserManagement.postRequest('UpdateUserManagement', data).subscribe((response) => {
        if (response.status == 1) {
          $('.close').trigger('click');
          this.Close();
          this.GetUserManagementList();
          this.successmessage("User updated Successfully");
          this.disabled = false;
        }
        else {
          this.Errormessage(response["message"]);
          this.disabled = false;
        }
      });
    }
  }


  deleteUser(user) {
    this.currentUserId = user.userId;
    this.deleteUserName = user.firstName;
  }


  deleteUserConfirm() {

    var data = {
      UserId: this.currentUserId,
    }

    this.UserManagement.postRequest('DeleteUserManagement', data).subscribe((response) => {
      if (response.status == 1) {
        this.GetUserManagementList();
        $('.modal-close').trigger('click');
        this.successmessage("User deleted successfully");
      }
      else {
        this.Errormessage(response["message"]);
      }
    });
  }

}
