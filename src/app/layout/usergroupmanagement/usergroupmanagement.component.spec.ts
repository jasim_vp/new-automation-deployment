import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsergroupmanagementComponent } from './usergroupmanagement.component';

describe('UsergroupmanagementComponent', () => {
  let component: UsergroupmanagementComponent;
  let fixture: ComponentFixture<UsergroupmanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsergroupmanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsergroupmanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
