import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from "@angular/forms";
import { formValidators } from "../../helper/formValidators";
import { NgForm } from '@angular/forms';
import { RoleDefinitionService } from '../../services/role-definition.service';
import { ServiceCatalogService } from 'src/app/services/service-catalog.service';
import { ProjectTypeRegulationService } from '../../services/project-type-regulation.service';
import { SharedvalueService } from 'src/app/services/sharedvalue.service';

declare var $: any
@Component({
  selector: 'app-role-definition',
  templateUrl: './role-definition.component.html',
  providers: [CommonService, MessageService, ToastModule],
  styleUrls: ['./role-definition.component.css']
})
export class RoleDefinitionComponent implements OnInit {
  cols: any[];
  roleDefinitions: any;
  stageDetails: any;
  roleDefinitionForm: FormGroup;
  showCount: boolean;
  disabled: boolean;
  selectedValue: any;
  serviceCataloDetails: any;
  editServiceCataloDetails: any;
  editData: any;
  role: string;
  roleDescription: string;
  uploadData: any;
  projectTypes: any;
  projectTypeListArray: any;
  projectTypeMapArray: any[];
  currentRoleId: number;
  deleteRoleName: string;
  descriptionPattern: string;
  isSubmitted: boolean = false;
  descriptionResult: boolean;
  

  // Stage
  stageList = [];
  stageSelectedItems = [];
  stageSettings = {};

  // Module
  moduleList = [];
  moduleSettings = {};
  moduleSelectedItems = [];

  // Sub Module
  subModuleList = [];
  subModuleSettings = {};
  subModuleSelectedItems = [];
  dubSubModuleSelectedItems = [];

  // Project Type
  projectTypeList = [];
  projectTypeSelectedItems = [];
  projectTypeSettings = {};

  result = [];
  subItemsAll = [];


  itemList = [];
  settings = {};
  user_id: number;
  showSideBar = true;

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private RoleDefinition: RoleDefinitionService,
    private messageService: MessageService,
    private serviceCatalog: ServiceCatalogService,
    private ProjectTypeRegulation: ProjectTypeRegulationService,
    public shrService: SharedvalueService
  ) { }

  ngOnInit() {
    $('.spinner').show();
    $('.modal').appendTo('#fullscreen');
    this.shrService.getSideBarDetail().subscribe(resp => { this.showSideBar = resp });

    this.selectedValue = 'true';
    this.GetRoleDefinitionList();
    this.descriptionPattern = "^[a-zA-Z0-9,.!? ]*$";
    this.roleDefinitionForm = this.formBuilder.group({
      Role: ["", [Validators.required, formValidators.alphabetical, formValidators.noWhitespace, Validators.maxLength(50)]],
      RoleDescription: ["", [Validators.required, formValidators.description, Validators.maxLength(50)]],
      Stage: ["", [Validators.required]],
      ProjectType: ["", [Validators.required]],
      Module: ["", [Validators.required]],
      SubModule: ["", [Validators.required]],
      previllege: this.formBuilder.array([]),
      active: "",
    });

    this.user_id = parseInt(localStorage.getItem('user_id'));
    this.cols = [
      { field: 'role', header: 'Role' },
      { field: 'roleDescription', header: 'Role Description' },
      { field: 'insertedDateTime', header: 'Created On' },
      { field: 'createdBy', header: 'Created By' },
    ];

    this.stageSettings = {
      singleSelection: true,
      text: "Select Stage",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };


    this.projectTypeSettings = {
      singleSelection: true,
      text: "Select Project Type",
      enableSearchFilter: true,
      classes: "myclass custom-class",
      showCheckbox: false
    };


    this.moduleSettings = {
      singleSelection: false,
      text: "Select Module",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class",
      badgeShowLimit: 2,

    };

    this.subModuleSettings = {
      singleSelection: false,
      text: "Select Sub Module",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      badgeShowLimit: 2,
      groupBy: "category"
    };

  }

  GetRoleDefinitionList() {
    $('.spinner').show();
    this.RoleDefinition.getRequest('GetRoleDefinitionList').subscribe((RoleDefinitionDetails) => {
      $('.spinner').hide();
      this.roleDefinitions = RoleDefinitionDetails;
      this.showCount = true;
    });
    this.GetServiceCatalogDetails();
  }

  isNumber(value: string | number): boolean {
    var result = ((value != null) &&
      (value !== '') &&
      !isNaN(Number(value.toString())));
    this.descriptionResult = result;
    return result;
  }


  onChange(moduleId: number, subModuleId: number, previllegeId: number, isChecked: boolean) {
    const previllegeArray = <FormArray>this.roleDefinitionForm.controls.previllege;
    if (isChecked) {
      $('.' + moduleId + '_' + subModuleId + '_' + previllegeId).prop('checked', true);
      previllegeArray.push(new FormControl(moduleId + '_' + subModuleId + '_' + previllegeId));
    } else {
      $('.' + moduleId + '_' + subModuleId + '_' + previllegeId).prop('checked', false);
      let index = previllegeArray.controls.findIndex(x => x.value == subModuleId)
      previllegeArray.removeAt(index);
    }
  }


  GetServiceCatalogDetails() {
    this.serviceCatalog.getRequest('GetServiceCatalogDetails').subscribe((ServiceCatalog) => {
      this.serviceCataloDetails = ServiceCatalog;
    });

    this.GetProjectTypeRegulationList();
  }


  GetProjectTypeRegulationList() {
    this.ProjectTypeRegulation.getRequest('GetProjectTypeRegulationList').subscribe((ProjectTypeRegulationDetails) => {
      this.projectTypes = ProjectTypeRegulationDetails;
      this.projectTypeList = [];
      this.projectTypes.forEach(resource => {
        this.projectTypeList.push({ id: resource['projectTypeId'], itemName: resource['projectType'] });
      });
    });
  }



  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }


  //Project type
  onProjectTypeItemDeSelect(item: any) {
    this.projectTypeSelectedItems = [];
    this.formUnchecked();
    this.clearList();
  }

  onProjectTypeItemDeSelectAll(items: any) {
    this.projectTypeSelectedItems = [];
    this.formUnchecked();
    this.clearList();
  }

  onProjectTypeFilterDeSelectAll(items: any) {
    this.onProjectTypeItemDeSelectAll(items);
  }

  onProjectTypeFilterSelectAll(items: any) {
    this.onProjectTypeItemSelect(items[items.length - 1]);
  }



  // Stage
  OnAddStageItemSelect(item: any) {
    this.moduleList = this.moduleListItems(item.id);
    this.moduleSelectedItems = [];
  }

  onAddStageDeSelectAll(items: any) {
    $('.card_div').hide();
    console.log(this.subModuleSelectedItems);
    for (let i = 0; i <= this.subModuleSelectedItems.length - 1; i++) {
      $('.subModule-' + this.subModuleSelectedItems[i]['id']).hide();
      this.subModulePrivilegeUnchecked(this.subModuleSelectedItems[i]['id'], 'add');
    }

    if (items.length == 0) {
      this.moduleList = [];
      this.moduleSelectedItems = [];
      this.subModuleSelectedItems = [];
      this.subModuleList = [];
      this.stageSelectedItems = [];
    }
  }

  onStageFilterSelectAll(items: any) {
    this.OnAddStageItemSelect(items[items.length - 1]);
  }

  onStageFilterDeSelectAll(items: any) {
    this.onAddStageDeSelectAll(items = []);
    this.moduleList = [];
  }



  // Module
  OnAddItemSelect(item: any) {
    $('.module-' + item.id).show();
    this.subModuleListItems(item.id);
  }


  OnAddItemDeSelect(item: any) {
    $('.module-' + item.id).hide();
    var sub = this.subModuleList;
    Object.keys(this.subModuleList).forEach(function (key) {
      if (item.itemName == sub[key]['category']) {
        delete sub[key];
      }
    });
    var newSubModules = sub.filter(o => Object.keys(o).length)
    this.subModuleList = newSubModules;
    this.submoduleSelectedItemRemove(this.subModuleSelectedItems, item.itemName);
  }

  submoduleSelectedItemRemove(dupSubModuleSelectedItems, groupName) {
    var sub = dupSubModuleSelectedItems;
    for (var key in dupSubModuleSelectedItems) {
      if (groupName == sub[key]['category']) {
        this.subModulePrivilegeUnchecked(sub[key]['id'], 'add')
        delete sub[key];
      }
    }

    var newSubModules = sub.filter(o => Object.keys(o).length);
    this.subModuleSelectedItems = newSubModules;
  }

  subModulePrivilegeUnchecked(subModuleId, operation) {
    if (operation == 'add') {
      $('.subModule-' + subModuleId).hide();
      var checkedValues = $('.subModule-' + subModuleId).find("input[name='checkbox']:checked").get();
      console.log(checkedValues);
      for (var checkedValueKey in checkedValues) {
        console.log(checkedValues[checkedValueKey]['value']);
        $('.' + checkedValues[checkedValueKey]['value']).prop('checked', false);
      }
    } else {
      $('.edit-subModule-' + subModuleId).hide();
      var checkedValues = $('.edit-subModule-' + subModuleId).find("input[name='checkbox']:checked").get();
      for (var checkedValueKey in checkedValues) {
        $('.' + checkedValues[checkedValueKey]['value']).prop('checked', false);
      }
    }
  }

  onAddSelectAll(items: any) {
    var dataIds = [];
    items.forEach(function (value, key) {
      $('.module-' + value.id).show();
      dataIds.push(value.id)
    });
    this.moduleSelectedItems = items;
    this.dubSubModuleSelectedItems = this.subModuleSelectedItems;
    this.subModuleSelectedItems = [];
    this.subModuleLoop(dataIds);
  }

  onAddFilterSelectAll(items: any) {
    items.forEach(resource => {
      this.OnAddItemSelect(resource);
    });
  }

  onAddFilterDeSelectAll(items: any) {
    items.forEach(resource => {
      this.OnAddItemDeSelect(resource);
    });
  }


  subModuleLoop(data) {
    this.subModuleList = [];
    for (let i = 0; i <= data.length - 1; i++) {
      this.subModuleList = this.subModuleListItems(data[i]);
    }
    this.subModuleSelectedItems = this.dubSubModuleSelectedItems;
  }


  moduleLoop(data) {
    for (let i = 0; i <= data.length - 1; i++) {
      this.moduleList = this.moduleListItems(data[i]);
    }
  }

  onAddDeSelectAll(items: any) {
    $('.card_div').hide();
    for (let i = 0; i <= this.subModuleSelectedItems.length - 1; i++) {
      $('.subModule-' + this.subModuleSelectedItems[i]['id']).hide();
      this.subModulePrivilegeUnchecked(this.subModuleSelectedItems[i]['id'], 'add');
    }

    if (items.length == 0) {
      this.moduleSelectedItems = [];
      this.subModuleSelectedItems = [];
      this.subModuleList = [];
    }
  }

  // Sub Module
  onAddSubItemSelect(items: any) {
    $('.subModule-' + items.id).show();
  }

  onAddSubItemDeSelect(items: any) {
    $('.subModule-' + items.id).hide();
    this.subModulePrivilegeUnchecked(items.id, 'add');
  }

  onAddSubSelectAll(items: any) {
    this.subItemsAll = [];
    items.forEach(function (value, key) {
      console.log(value);
      $('.subModule-' + value.id).show();
    });
    this.subItemsAll = items;
    this.subModuleSelectedItems = items;
  }

  onAddSubDeSelectAll(items: any) {
    for (let i = 0; i <= this.subModuleSelectedItems.length - 1; i++) {
      $('.subModule-' + this.subModuleSelectedItems[i]['id']).hide();
      this.subModulePrivilegeUnchecked(this.subModuleSelectedItems[i]['id'], 'add');
    }
    if (items.length == 0) {
      this.subModuleSelectedItems = [];
    }
  }

  onAddSubGroupSelect(items: any) {
    items.list.forEach(resource => {
      $('.subModule-' + resource.id).show();
    });
  }


  onAddSubGroupDeSelect(items: any) {
    items.list.forEach(resource => {
      $('.subModule-' + resource.id).hide();
      this.subModulePrivilegeUnchecked(resource.id, 'add');
    })
  }

  // Project Type
  onProjectTypeItemSelect(item: any) {
    $('.card_div').hide();
    this.clearList();
    this.RoleDefinition.getRequest('GetProjectTypeWiseArray/' + item.id).subscribe(projectTypeDetailsArray => {
      this.projectTypeListArray = projectTypeDetailsArray;

      var arrayFormData = [];
      let stages = {};
      let module_array = {};
      var sub_module_array = {};
      if (this.projectTypeListArray != null) {
        this.projectTypeListArray.forEach(resource => {

          if (!stages.hasOwnProperty(resource.stageId)) {
            stages[resource.stageId] = {};
            stages[resource.stageId]['stageId'] = resource.stageId;
            stages[resource.stageId]['stageName'] = resource.stageName;
            stages[resource.stageId]['modules'] = {};
          }


          if (!stages[resource.stageId]['modules'].hasOwnProperty(resource.moduleId)) {
            stages[resource.stageId]['modules'][resource.moduleId] = {};
            stages[resource.stageId]['modules'][resource.moduleId]['moduleId'] = resource.moduleId;;
            stages[resource.stageId]['modules'][resource.moduleId]['moduleName'] = resource.moduleName;
            stages[resource.stageId]['modules'][resource.moduleId]['subModules'] = {};
          }

          if (!stages[resource.stageId]['modules'][resource.moduleId]['subModules'].hasOwnProperty(resource.subModuleId)) {
            stages[resource.stageId]['modules'][resource.moduleId]['subModules'][resource.subModuleId] = {};
            stages[resource.stageId]['modules'][resource.moduleId]['subModules'][resource.subModuleId]['subModuleId'] = resource.subModuleId;;
            stages[resource.stageId]['modules'][resource.moduleId]['subModules'][resource.subModuleId]['subModuleName'] = resource.subModuleName;
          }
        });
      }

      arrayFormData['stages'] = stages;
      this.projectTypeMapArray = arrayFormData;
      var modulesDropDownList = [];
      var subModuleDropDownList = [];
      var stageDropDownList = [];
      var stageIds = [];
      var moduleIds = [];
      Object.keys(arrayFormData['stages']).forEach(function (stageKey) {
        stageIds.push(arrayFormData['stages'][stageKey]['stageId']);
        stageDropDownList.push({
          id: arrayFormData['stages'][stageKey]['stageId'], itemName: arrayFormData['stages'][stageKey]['stageName'],
        });

        Object.keys(arrayFormData['stages'][stageKey]['modules']).forEach(function (moduleKey) {
          moduleIds.push(arrayFormData['stages'][stageKey]['modules'][moduleKey]['moduleId']);
          modulesDropDownList.push({
            id: arrayFormData['stages'][stageKey]['modules'][moduleKey]['moduleId'], itemName: arrayFormData['stages'][stageKey]['modules'][moduleKey]['moduleName'],
          });

          Object.keys(arrayFormData['stages'][stageKey]['modules'][moduleKey]['subModules']).forEach(function (subModuleKey) {
            subModuleDropDownList.push({
              id: arrayFormData['stages'][stageKey]['modules'][moduleKey]['subModules'][subModuleKey]['subModuleId'], itemName: arrayFormData['stages'][stageKey]['modules'][moduleKey]['subModules'][subModuleKey]['subModuleName'], category: arrayFormData['stages'][stageKey]['modules'][moduleKey]['moduleName']
            });
          });
        });
      });

      this.stageList = stageDropDownList;

    });

  }





  submitForm(formData) {
    this.validateAllFormFields(this.roleDefinitionForm);
    this.isSubmitted = true;
    this.isNumber(formData["RoleDescription"]);
    if (this.roleDefinitionForm.valid && this.descriptionResult == false) {
      this.disabled = true;
      var filteredPrivilege = formData["previllege"].filter(function (el) {
        return el != null;
      });

      var data = [];
      if (filteredPrivilege.length > 0) {
        for (let k = 0; k <= filteredPrivilege.length - 1; k++) {
          let previlleageData = filteredPrivilege[k].split('_');
          data.push({
            UserId: this.user_id,
            ProjectTypeId: formData['ProjectType'][0]['id'],
            StageId: formData["Stage"][0]["id"],
            SubModuleId: previlleageData[1] * 1,
            PrivilegeId: previlleageData[2] * 1,
            Role: formData["Role"],
            flag: 0,
            Active: (formData["active"] == "true"),
            RoleDescription: formData["RoleDescription"]
          });
        }
      }

      if (data.length > 0) {
        this.RoleDefinition.postRequest('InsertRoleDefinition', data).subscribe((response) => {
          if (response.status == 1) {
            $('.close').trigger('click');
            this.Close();
            this.GetRoleDefinitionList();
            this.successmessage("Role Definition created successfully");
            this.disabled = false;
            this.isSubmitted = false;
          }
          else {
            if (response.status == 2) {
              this.Errormessage(response.message);
              this.disabled = false;
            } else {
              this.Errormessage(response.message);
              this.disabled = false;
            }
          }
        });
      } else {
        this.Errormessage('Please select previlege');
        this.disabled = false;
      }


    }
  }




  formUnchecked() {
    $.each($("input[name='checkbox']:checked"), function () {
      let previlleageVal = $(this).val().split('_');
      $('.module-' + previlleageVal[0]).hide();
      $('.subModule-' + previlleageVal[1]).hide();
      var val = $(this).val();
      $('.' + val).prop("checked", false);
    });
  }



  moduleListItems(id) {
    this.moduleList = [];
    for (var stageKey in this.projectTypeMapArray['stages']) {
      for (var moduleKey in this.projectTypeMapArray['stages'][stageKey]['modules']) {
        if (id == stageKey) {
          let moduleId = this.projectTypeMapArray['stages'][stageKey]['modules'][moduleKey]['moduleId'];
          let moduleName = this.projectTypeMapArray['stages'][stageKey]['modules'][moduleKey]['moduleName'];
          this.moduleList.push({
            id: moduleId, itemName: moduleName
          });
        }
      }
    }

    return this.moduleList;
  }




  subModuleListItems(id) {
    for (var stageKey in this.projectTypeMapArray['stages']) {
      for (var moduleKey in this.projectTypeMapArray['stages'][stageKey]['modules']) {
        for (var subModulekey in this.projectTypeMapArray['stages'][stageKey]['modules'][moduleKey]['subModules']) {
          if (id == moduleKey) {
            let subModuleId = this.projectTypeMapArray['stages'][stageKey]['modules'][moduleKey]['subModules'][subModulekey]['subModuleId'];
            let subModuleName = this.projectTypeMapArray['stages'][stageKey]['modules'][moduleKey]['subModules'][subModulekey]['subModuleName'];
            let moduleName = this.projectTypeMapArray['stages'][stageKey]['modules'][moduleKey]['moduleName'];
            this.subModuleList.push({
              id: subModuleId, itemName: subModuleName, category: moduleName
            });
          }
        }
      }
    }
    let cloneSubModuleList = [...this.subModuleList];
    this.subModuleList = [];
    this.subModuleList = cloneSubModuleList;
    return this.subModuleList;
  }


  editRoleDefinition(roleDefinitions: any) {
    $('.spinner').show();
    this.RoleDefinition.getRequest('GetRoleDefinitionListById/' + roleDefinitions.roleId + '').subscribe(EditRoleDefinitionDetails => {
      this.editData = EditRoleDefinitionDetails;
      let modules = {};
      var resultData = [];
      var previlegeIds = [];
      var sub_module_array = {};
      var datsIds2 = [];
      var i = 0;

      if (this.editData != null && this.editData.length > 0) {
        this.editData.forEach(resource => {

          resultData['roleId'] = resource.roleId;
          resultData['role'] = resource.role;
          resultData['roleDescription'] = resource.roleDescription;
          resultData['stageId'] = resource.stageId;
          resultData['stageName'] = resource.stageName;
          resultData['projectTypeId'] = resource.projectTypeId;
          resultData['projectType'] = resource.projectTypeName;

          if (!modules.hasOwnProperty(resource.moduleId)) {
            modules[resource.moduleId] = {};
            modules[resource.moduleId]['moduleId'] = resource.moduleId;
            modules[resource.moduleId]['moduleName'] = resource.moduleName;
            modules[resource.moduleId]['subModules'] = {};
          }


          if (!modules[resource.moduleId]['subModules'].hasOwnProperty(resource.subModuleId)) {
            modules[resource.moduleId]['subModules'][resource.subModuleId] = {};
            modules[resource.moduleId]['subModules'][resource.subModuleId]['subModuleId'] = resource.subModuleId;
            modules[resource.moduleId]['subModules'][resource.subModuleId]['subModuleName'] = resource.subModuleName;
            modules[resource.moduleId]['subModules'][resource.subModuleId]['privilleges'] = {};
            sub_module_array[resource.subModuleId] = {};
          }

          sub_module_array[resource.subModuleId][i] = {};
          sub_module_array[resource.subModuleId][i] = { 'privilegeId': resource.privilegeId };

          modules[resource.moduleId]['subModules'][resource.subModuleId]['privilleges'] = Object.values(sub_module_array[resource.subModuleId]);
          i++;

        });

        resultData['modules'] = modules;
        this.result = resultData;

        console.log(this.editData);
        for (var stageKey in this.serviceCataloDetails) {
          for (var moduleKey in this.serviceCataloDetails[stageKey]['modules']) {
            for (var subModuleKey in this.serviceCataloDetails[stageKey]['modules'][moduleKey]['submodules']) {
              for (var privilegeKey in this.serviceCataloDetails[stageKey]['modules'][moduleKey]['submodules'][subModuleKey]['privileges']) {
                var loopModuleId = this.serviceCataloDetails[stageKey]['modules'][moduleKey]['moduleId'];

                if (resultData['modules'][loopModuleId] != undefined && loopModuleId == resultData['modules'][loopModuleId]['moduleId']) {
                  this.serviceCataloDetails[stageKey]['modules'][moduleKey]['selected'] = true;
                  var loopSubModuleId = this.serviceCataloDetails[stageKey]['modules'][moduleKey]['submodules'][subModuleKey]['subModuleId'];
                  if (resultData['modules'][loopModuleId]['subModules'][loopSubModuleId] != undefined && resultData['modules'][loopModuleId]['subModules'][loopSubModuleId]['subModuleId'] == loopSubModuleId) {
                    this.serviceCataloDetails[stageKey]['modules'][moduleKey]['submodules'][subModuleKey]['selected'] = true;
                    var loopPrivilegeId = this.serviceCataloDetails[stageKey]['modules'][moduleKey]['submodules'][subModuleKey]['privileges'][privilegeKey]['privilegeId'];
                    var checkExists = resultData['modules'][loopModuleId]['subModules'][loopSubModuleId]['privilleges'].filter(function (p) { return p.privilegeId == loopPrivilegeId });
                    if (checkExists.length > 0) {
                      this.serviceCataloDetails[stageKey]['modules'][moduleKey]['submodules'][subModuleKey]['privileges'][privilegeKey]['selected'] = true;
                      var id = 'edit_' + loopModuleId + '_' + loopSubModuleId + '_' + loopPrivilegeId;
                      $('#' + id).prop('checked', true);

                    } else {
                      this.serviceCataloDetails[stageKey]['modules'][moduleKey]['submodules'][subModuleKey]['privileges'][privilegeKey]['selected'] = false;
                    }
                  } else {
                    //$('#' + id).prop('checked', false);
                    this.serviceCataloDetails[stageKey]['modules'][moduleKey]['submodules'][subModuleKey]['selected'] = false;
                    this.serviceCataloDetails[stageKey]['modules'][moduleKey]['submodules'][subModuleKey]['privileges'][privilegeKey]['selected'] = false;
                  }

                } else {
                  this.serviceCataloDetails[stageKey]['modules'][moduleKey]['selected'] = false;
                }
              }
            }
          }
        }

        this.editServiceCataloDetails = this.serviceCataloDetails;

        this.role = resultData["role"];
        this.roleDescription = resultData["roleDescription"];

        this.projectTypeSelectedItems = [
          { id: resultData['projectTypeId'], itemName: resultData['projectType'] }
        ];

        this.stageSelectedItems = [
          { id: resultData['stageId'], itemName: resultData['stageName'] }
        ];

        var modulesDropdown = [];
        var subModuleDropDown = [];
        Object.keys(resultData['modules']).forEach(function (moduleKey) {
          $('.module-' + resultData['modules'][moduleKey]['moduleId']).show();
          datsIds2.push(resultData['modules'][moduleKey]['moduleId']);
          modulesDropdown.push({
            id: resultData['modules'][moduleKey]['moduleId'], itemName: resultData['modules'][moduleKey]['moduleName'],
          });

          Object.keys(resultData['modules'][moduleKey]['subModules']).forEach(function (subModuleKey) {
            $('.subModule-' + resultData['modules'][moduleKey]['subModules'][subModuleKey]['subModuleId']).show();
            subModuleDropDown.push({
              id: resultData['modules'][moduleKey]['subModules'][subModuleKey]['subModuleId'], itemName: resultData['modules'][moduleKey]['subModules'][subModuleKey]['subModuleName'], category: resultData['modules'][moduleKey]['moduleName']
            });
          });
        });


        this.RoleDefinition.getRequest('GetProjectTypeWiseArray/' + resultData['projectTypeId']).subscribe(projectTypeDetailsArray => {
          this.projectTypeListArray = projectTypeDetailsArray;

          var arrayFormData = [];
          let stages = {};
          let module_array = {};
          var sub_module_array = {};
          if (this.projectTypeListArray != null) {
            this.projectTypeListArray.forEach(resource => {

              if (!stages.hasOwnProperty(resource.stageId)) {
                stages[resource.stageId] = {};
                stages[resource.stageId]['stageId'] = resource.stageId;
                stages[resource.stageId]['stageName'] = resource.stageName;
                stages[resource.stageId]['modules'] = {};
              }


              if (!stages[resource.stageId]['modules'].hasOwnProperty(resource.moduleId)) {
                stages[resource.stageId]['modules'][resource.moduleId] = {};
                stages[resource.stageId]['modules'][resource.moduleId]['moduleId'] = resource.moduleId;;
                stages[resource.stageId]['modules'][resource.moduleId]['moduleName'] = resource.moduleName;
                stages[resource.stageId]['modules'][resource.moduleId]['subModules'] = {};
              }

              if (!stages[resource.stageId]['modules'][resource.moduleId]['subModules'].hasOwnProperty(resource.subModuleId)) {
                stages[resource.stageId]['modules'][resource.moduleId]['subModules'][resource.subModuleId] = {};
                stages[resource.stageId]['modules'][resource.moduleId]['subModules'][resource.subModuleId]['subModuleId'] = resource.subModuleId;;
                stages[resource.stageId]['modules'][resource.moduleId]['subModules'][resource.subModuleId]['subModuleName'] = resource.subModuleName;
              }
            });
          }

          arrayFormData['stages'] = stages;
          this.projectTypeMapArray = arrayFormData;
          var modulesDropDownList = [];
          var subModuleDropDownList = [];
          var stageDropDownList = [];
          var stageIds = [];
          var moduleIds = [];
          Object.keys(arrayFormData['stages']).forEach(function (stageKey) {
            stageIds.push(arrayFormData['stages'][stageKey]['stageId']);
            stageDropDownList.push({
              id: arrayFormData['stages'][stageKey]['stageId'], itemName: arrayFormData['stages'][stageKey]['stageName'],
            });

            Object.keys(arrayFormData['stages'][stageKey]['modules']).forEach(function (moduleKey) {
              moduleIds.push(arrayFormData['stages'][stageKey]['modules'][moduleKey]['moduleId']);
              modulesDropDownList.push({
                id: arrayFormData['stages'][stageKey]['modules'][moduleKey]['moduleId'], itemName: arrayFormData['stages'][stageKey]['modules'][moduleKey]['moduleName'],
              });

              Object.keys(arrayFormData['stages'][stageKey]['modules'][moduleKey]['subModules']).forEach(function (subModuleKey) {
                subModuleDropDownList.push({
                  id: arrayFormData['stages'][stageKey]['modules'][moduleKey]['subModules'][subModuleKey]['subModuleId'], itemName: arrayFormData['stages'][stageKey]['modules'][moduleKey]['subModules'][subModuleKey]['subModuleName'], category: arrayFormData['stages'][stageKey]['modules'][moduleKey]['moduleName']
                });
              });
            });
          });

          this.stageList = stageDropDownList;
          this.moduleList = [];
          this.moduleLoop([resultData['stageId']]);
          this.subModuleList = [];
          this.subModuleLoop(datsIds2);

        });

        this.moduleSelectedItems = modulesDropdown;
        this.subModuleSelectedItems = subModuleDropDown;
        this.dubSubModuleSelectedItems = subModuleDropDown;
        this.selectedValue = String(roleDefinitions["active"]);
        this.uploadData = EditRoleDefinitionDetails;
        $('.spinner').hide();

      } else {
        this.Errormessage('Role mapping data is empty');
      }
    });
  }



  updateForm(formData) {
    this.validateAllFormFields(this.roleDefinitionForm);
    this.isSubmitted = true;
    this.isNumber(formData["RoleDescription"]);
    if (this.roleDefinitionForm.valid && this.descriptionResult == false) {
      this.disabled = true;
      var filteredPrivilege = formData["previllege"].filter(function (el) {
        return el != null;
      });

      var array = [];
      $.each($("input[name='checkbox']:checked"), function () {
        array.push($(this).attr("value"));
      });

      var newFilterArr = this.getUnique(array.concat(filteredPrivilege));
      var data = [];
      if (newFilterArr.length > 0) {
        for (let k = 0; k <= newFilterArr.length - 1; k++) {
          let previlleageData = newFilterArr[k].split('_');
          data.push({
            RoleId: this.result['roleId'],
            UserId: this.user_id,
            ProjectTypeId: formData['ProjectType'][0]['id'],
            StageId: formData["Stage"][0]["id"],
            SubModuleId: previlleageData[1] * 1,
            PrivilegeId: previlleageData[2] * 1,
            flag: 1,
            Active: (formData["active"] == "true"),
            Role: formData["Role"],
            RoleDescription: formData["RoleDescription"]
          });
        }
      }


      if (data.length > 0) {
        this.RoleDefinition.postRequest('UpdateRoleDefinition', data).subscribe((response) => {
          if (response.status == 1) {
            $('.close').trigger('click');
            this.Close();
            this.GetRoleDefinitionList();
            this.successmessage("Role Definition updated successfully");
            this.disabled = false;
            this.isSubmitted = false;
          }
          else {
            if (response.status == 2) {
              this.Errormessage(response.message);
              this.disabled = false;
            } else {
              this.Errormessage(response.message);
            }
          }
        });
      } else {
        this.Errormessage('Please select previlege');
        this.disabled = false;
      }


    }
  }


  deleteRole(role) {
    this.currentRoleId = role.roleId;
    this.deleteRoleName = role.role;
  }

  deleteRoleConfirm() {

    this.RoleDefinition.getRequest('DeleteRoleDefinition/' + this.currentRoleId).subscribe((response) => {
      if (response.status == 1) {
        this.GetRoleDefinitionList();
        $('.modal-close').trigger('click');
        this.successmessage("Role definition deleted successfully");
      }
      else {
        if (response["status"] == 2) {
          this.Errormessage('Role definition already exists');
        }else{
          this.Errormessage(response.message);
        }
      }
    });
  }


  getUnique(array) {
    var uniqueArray = [];

    // Loop through array values
    for (let i = 0; i < array.length; i++) {
      if (uniqueArray.indexOf(array[i]) === -1) {
        uniqueArray.push(array[i]);
      }
    }
    return uniqueArray;
  }




  public get getFields() {
    return this.roleDefinitionForm.controls;
  }

  Open() {
    this.selectedValue = 'true';
  }

  Close() {

    this.isSubmitted = false;
    for (let i = 0; i <= this.moduleSelectedItems.length - 1; i++) {
      $('.module-' + this.moduleSelectedItems[i]['id']).hide();
    }

    for (let i = 0; i <= this.subModuleSelectedItems.length - 1; i++) {
      $('.subModule-' + this.subModuleSelectedItems[i]['id']).hide();
    }

    this.roleDefinitionForm.reset();
    this.clearList();
    $.each($("input[name='checkbox']:checked"), function () {
      let previlleageVal = $(this).val().split('_');
      $('.module-' + previlleageVal[0]).hide();
      $('.subModule-' + previlleageVal[1]).hide();
      var val = $(this).val();
      $('.' + val).prop("checked", false);
    });
  }

  clearList() {
    this.stageList = [];
    this.moduleList = [];
    this.subModuleList = [];
    this.stageSelectedItems = [];
    this.moduleSelectedItems = [];
    this.subModuleSelectedItems = [];
  }

  successmessage(message) {
    this.messageService.add({ severity: 'success', summary: 'Success', detail: message });
  }

  Errormessage(errorsmessage) {
    this.messageService.add({ severity: 'error', summary: 'Error', detail: errorsmessage });
  }

}
