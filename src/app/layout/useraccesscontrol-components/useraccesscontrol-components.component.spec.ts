import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UseraccesscontrolComponentsComponent } from './useraccesscontrol-components.component';

describe('UseraccesscontrolComponentsComponent', () => {
  let component: UseraccesscontrolComponentsComponent;
  let fixture: ComponentFixture<UseraccesscontrolComponentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UseraccesscontrolComponentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UseraccesscontrolComponentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
