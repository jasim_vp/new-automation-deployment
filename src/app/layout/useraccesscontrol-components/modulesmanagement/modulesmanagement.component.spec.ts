import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModulesmanagementComponent } from './modulesmanagement.component';

describe('ModulesmanagementComponent', () => {
  let component: ModulesmanagementComponent;
  let fixture: ComponentFixture<ModulesmanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModulesmanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModulesmanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
