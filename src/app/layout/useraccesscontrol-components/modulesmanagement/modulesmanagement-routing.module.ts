import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModulesmanagementComponent } from '../modulesmanagement/modulesmanagement.component';
import { SharedModule } from '../../../shared.module';
const routes: Routes = [
    {
        path: '',
        component: ModulesmanagementComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes),SharedModule],
    exports: [RouterModule]
})
export class ModulesRoutingModule {}
