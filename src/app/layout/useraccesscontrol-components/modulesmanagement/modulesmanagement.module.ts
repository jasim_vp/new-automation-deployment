import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModulesRoutingModule } from './modulesmanagement-routing.module';
import { ModulesmanagementComponent } from '../modulesmanagement/modulesmanagement.component';
import {
    ReactiveFormsModule,
    FormsModule,
    FormGroup,
    FormControl,
    Validators,
    FormBuilder
  } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule, ModulesRoutingModule, FormsModule,ReactiveFormsModule
  ],
  declarations: [ModulesmanagementComponent]
})
export class ModulesManagementModule { }

