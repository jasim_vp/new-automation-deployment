import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { CommonService } from '../../../services/common.service';
import { MenulistService } from '../../../services/menulist.service';
import { HttpClient } from '@angular/common/http';
import { formValidators } from "../../../helper/formValidators";
//import { MessageserviceService } from '../../../services/messageservice.service';
import {MessageService} from 'primeng/api';
import {ToastModule} from 'primeng/toast';
import { NgForm } from '@angular/forms';

declare var $: any;
@Component({
  selector: 'app-modulesmanagement',
  templateUrl: './modulesmanagement.component.html',
  providers: [CommonService,MessageService,ToastModule],
  styleUrls: ['./modulesmanagement.component.css']
})
export class ModulesmanagementComponent implements OnInit {
  menuList: any;
  menuForm: FormGroup;
  isSubmitted = false;
  selectedValue: any;
  cols: any[];
  show: boolean;
  menuName: string;
  uploadData: any;
  disadble:boolean;
  showmenu: boolean;
  constructor(private http: HttpClient, private toast: ToastModule, private message: MessageService, private formBuilder: FormBuilder, private MenuList: MenulistService) { }

  ngOnInit() {
    this.selectedValue = 'true';
    this.disadble = false;
    this.show = true;
    this.menuList = { list: [], params: { total: 0 } };
    this.menuForm = this.formBuilder.group({
      MenuName: ["", [Validators.required, formValidators.alphabetical, formValidators.noWhitespace, Validators.maxLength(50)]],
      active: "",
      rememberMeFlag: [false]
    });


    this.cols = [
      { field: 'menuname', header: 'Main Menu' },
      { field: 'createdBy', header: 'Created By' },
      { field: 'insertedTime', header: 'Created On' }
      // { field: 'active', header: 'Status' }
    ];

    this.GetMenuList();
  }

  GetMenuList() {
    this.MenuList.getRequest('GetMenuList').subscribe((Menudetails) => {
      //debugger
      this.menuList = Menudetails;
      this.showmenu = true;
      //alert(JSON.stringify(this.menuList))
    });
  }

  public get getFields() {
    return this.menuForm.controls;
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  submitForm(formData) {
    //debugger
    this.disadble = true;
    this.validateAllFormFields(this.menuForm);
    if (this.menuForm.valid) {
      var data = {
        Menuname: formData["MenuName"],
        InsertedBy: 1,
        Active: (formData["active"] == "true")
        //Active:1
      }
      this.MenuList.postRequest('InsertMenuList', data).subscribe((response) => {
        //debugger
        if (response.status == 1) {
          $('.close').trigger('click');
          this.Close();
          this.GetMenuList();
          this.successmessage("Module Inserted Successfully");
          this.disadble = false;
        }
        else {
          //debugger
          this.disadble = false;
          this.Errormessage(response["message"]);
        }
      });
    }
    else {
      this.disadble = false;
    }

  }

  UpdateForm(formData) {
    this.disadble = true;
    this.validateAllFormFields(this.menuForm);
    if (this.menuForm.valid) {
      var data = {
        MenuId: this.uploadData["menuId"],
        Menuname: formData["MenuName"],
        InsertedBy: 1,
        UpdatedBy: 1,
        InsertedTime: this.uploadData["insertedTime"],
        Active: (formData["active"] == "true")
      }
      this.MenuList.postRequest('UpdatedMenuList', data).subscribe((response) => {
        if (response.status == 1) {
          $('.close').trigger('click');
          this.Close();
          this.GetMenuList();
          this. successmessage('Module updated successfully');
          this.disadble = false;
        }
        else {
          this.disadble = false;
          this.Errormessage(response["message"]);          
        }
      });
    }
    else {
      this.disadble = false;
    }
  }

  editMenuList(menu: any) {
    //debugger
    //alert(JSON.stringify(menu));
    this.menuName = menu["menuname"];
    this.selectedValue = String(menu["active"])
    this.uploadData = menu;
  }

  Open() {
    this.disadble = false;
    this.selectedValue = 'true';
  }

  Close() {
    this.menuForm.reset();
  }

  successmessage(message){
    this.message.add({severity:'success', summary:'Success Message', detail:message});
  }

  Errormessage(errorsmessage){
    this.message.add({severity:'error', summary:'', detail:errorsmessage});
  }

  clear() {
    this.message.clear();
}

  exportPdf() {
    //debugger
    //alert(JSON.stringify(this.menuList));
    import("jspdf").then(jsPDF => {
      import("jspdf-autotable").then(x => {
        const doc = new jsPDF.default(0, 0);
        doc.autoTable(this.cols, this.menuList);
        doc.save('primengTable.pdf');
      })
    })
  }

  exportExcel() {
    //debugger
    var data = [];
    for( let i = 0; i <= this.menuList.length-1; i++){
      data.push({MenuName:this.menuList[i].menuname,CreatedBy:this.menuList[i].insertedBy,CreatedOn:this.menuList[i].insertedTime,Active:this.menuList[i].active});
     // console.log(JSON.stringify(data+'mmmmmmmmmmmmmm'))
    }
    
    alert(JSON.stringify(data));
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(data); 
      const workbook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, "Modules_Details");
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    import("file-saver").then(FileSaver => {
      let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
      let EXCEL_EXTENSION = '.xlsx';
      const data: Blob = new Blob([buffer], {
        type: EXCEL_TYPE
      });
      FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
    });
  }
  
}

export class uploadForm {
  public MenuId: any;
  public Menuname: string;
  public InsertedBy: string;
  public Active: boolean;
}