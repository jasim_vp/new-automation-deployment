import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from "@angular/forms";
import { formValidators } from "../../helper/formValidators";
import { NgForm } from '@angular/forms';
import { ProjectTypeRegulationService } from '../../services/project-type-regulation.service';
import { ServiceCatalogService } from 'src/app/services/service-catalog.service';
import { SharedvalueService } from 'src/app/services/sharedvalue.service';
declare var $: any
@Component({
  selector: 'app-project-type-regulation',
  templateUrl: './project-type-regulation.component.html',
  providers: [CommonService, MessageService, ToastModule],
  styleUrls: ['./project-type-regulation.component.css']
})

export class ProjectTypeRegulationComponent implements OnInit {

  cols: any[];
  projectTypeRegulationForm: FormGroup;
  projectTypeRegulation: any;
  serviceCataloDetails: any;
  showCount: boolean;
  disabled: boolean;
  selectedValue: any;
  user_id: number;
  projectType: string;
  projectTypeDescription: string;
  uploadData: any;
  editData: any;
  result = [];
  currentProjectTypeId: number;
  deleteProjectTypeName: string
  descriptionResult: boolean;
  descriptionPattern: string;
  isSubmitted: boolean = false;


  // Stage 
  stageList = [];
  stageSelectedItems = [];
  stageSettings = {};

  // Module
  moduleList = [];
  moduleSettings = {};
  moduleSelectedItems = [];

  // Sub Module
  subModuleList = [];
  subModuleSettings = {};
  subModuleSelectedItems = [];
  dupSubModuleSelectedItems = [];
  showSideBar = true;


  constructor(private shrService: SharedvalueService, private http: HttpClient, private formBuilder: FormBuilder, private messageService: MessageService, private serviceCatalog: ServiceCatalogService, private ProjectTypeRegulation: ProjectTypeRegulationService, ) { }

  ngOnInit() {
    $('.spinner').show();

    this.shrService.getSideBarDetail().subscribe(resp => { this.showSideBar = resp });
    this.descriptionPattern = "^[a-zA-Z0-9,.!? ]*$";
    this.selectedValue = 'true';
    this.GetProjectTypeRegulationList();

    this.projectTypeRegulationForm = this.formBuilder.group({
      ProjectType: ["", [Validators.required, formValidators.alphabetical, formValidators.noWhitespace, Validators.maxLength(50)]],
      ProjectTypeDescription: ["", [Validators.required, formValidators.description, Validators.maxLength(50)]],
      Stage: ["", [Validators.required]],
      Module: ["", [Validators.required]],
      SubModule: ["", [Validators.required]],
      active: "",
    });


    this.user_id = parseInt(localStorage.getItem('user_id'));
    this.cols = [
      { field: 'projectType', header: 'Project Type' },
      { field: 'projectTypeDescription', header: 'Description' },
      { field: 'insertedDateTime', header: 'Created On' },
      { field: 'createdBy', header: 'Created By' },
    ];


    this.stageSettings = {
      singleSelection: true,
      text: "Select Stage",
      enableSearchFilter: true,
      classes: "myclass custom-class"
    };


    this.moduleSettings = {
      singleSelection: false,
      text: "Select Module",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      classes: "myclass custom-class",
      badgeShowLimit: 2
    };


    this.subModuleSettings = {
      singleSelection: false,
      text: "Select Sub Module",
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      enableSearchFilter: true,
      badgeShowLimit: 2,
      groupBy: "category"
    };



  }


  GetProjectTypeRegulationList() {
    
    this.ProjectTypeRegulation.getRequest('GetProjectTypeRegulationList').subscribe((ProjectTypeRegulationDetails) => {

      var objectsData = ProjectTypeRegulationDetails.sort(function (a, b) {
        return a.projectTypeId - b.projectTypeId;
      });

      this.projectTypeRegulation = objectsData.reverse();
      this.showCount = true;
      $('.spinner').hide();
    },err =>{
      $('.spinner').hide();

    });

    this.GetServiceCatalogDetails();
  }






  GetServiceCatalogDetails() {
    this.serviceCatalog.getRequest('GetServiceCatalogDetails').subscribe((ServiceCatalog) => {
      this.serviceCataloDetails = ServiceCatalog;
      this.stageList = [];
      for (let i = 0; i <= ServiceCatalog.length - 1; i++) {
        this.stageList.push({
          id: ServiceCatalog[i]['stageID'], itemName: ServiceCatalog[i]['stageName']
        });
      }
    });
  }


  // Stage
  OnAddStageItemSelect(item: any) {
    this.moduleList = this.moduleListItems(item.id);
    this.moduleSelectedItems = [];
    this.subModuleList = [];
    this.subModuleSelectedItems = [];
  }


  onStageFilterSelectAll(items: any) {
    this.OnAddStageItemSelect(items[items.length - 1]);
  }

  onStageFilterDeSelectAll(items: any) {
    this.onAddStageDeSelectAll(items = []);
  }


  onAddStageDeSelect(item: any) {
    this.moduleList = [];
    this.moduleSelectedItems = [];
    this.subModuleList = [];
    this.subModuleSelectedItems = [];
  }

  onAddStageDeSelectAll(items: any) {
    if (items.length == 0) {
      this.stageSelectedItems = [];
      this.moduleList = [];
      this.moduleSelectedItems = [];
      this.subModuleList = [];
      this.subModuleSelectedItems = [];
    }
  }


  OnAddItemSelect(item: any) {
    this.subModuleList = this.subModuleListItems(item.id);
  }

  OnAddItemDeSelect(item: any) {
    var sub = this.subModuleList;
    Object.keys(this.subModuleList).forEach(function (key) {
      if (item.itemName == sub[key]['category']) {
        delete sub[key];
      }
    });

    var newSubModules = sub.filter(o => Object.keys(o).length)
    this.subModuleList = newSubModules;
    this.dupSubmoduleSelectedItemRemove(this.subModuleSelectedItems, item.itemName);
  }


  dupSubmoduleSelectedItemRemove(dupSubModuleSelectedItems, groupName) {
    var sub = dupSubModuleSelectedItems;
    Object.keys(dupSubModuleSelectedItems).forEach(function (key) {
      if (groupName == sub[key]['category']) {
        delete sub[key];
      }
    });

    var newSubModules = sub.filter(o => Object.keys(o).length)
    this.subModuleSelectedItems = newSubModules;
  }

  onAddSelectAll(items: any) {
    var dataIds = [];
    items.forEach(function (value, key) {
      dataIds.push(value.id)
    });
    this.dupSubModuleSelectedItems = this.subModuleSelectedItems;
    this.subModuleSelectedItems = [];
    this.subModuleLoop(dataIds);
  }


  onAddFilterSelectAll(items: any) {
    items.forEach(resource => {
      this.OnAddItemSelect(resource);
    });
  }


  onAddFilterDeSelectAll(items: any) {
    items.forEach(resource => {
      this.OnAddItemDeSelect(resource);
    });
  }


  subModuleLoop(data) {
    this.subModuleList = [];
    for (let i = 0; i <= data.length - 1; i++) {
      this.subModuleList = this.subModuleListItems(data[i]);
    }
    this.subModuleSelectedItems = this.dupSubModuleSelectedItems;
  }


  onAddDeSelectAll(items: any) {
    if (items.length == 0) {
      this.moduleSelectedItems = [];
      this.subModuleSelectedItems = [];
      this.subModuleList = [];
    }
  }


  onAddSubDeSelectAll(items: any) {
    if (items.length == 0) {
      this.subModuleSelectedItems = [];
    }
  }



  moduleListItems(itemId) {
    this.moduleList = [];
    for (let i = 0; i <= this.serviceCataloDetails.length - 1; i++) {
      if (this.serviceCataloDetails[i]['modules'].length > 0) {
        for (let j = 0; j <= this.serviceCataloDetails[i]['modules'].length - 1; j++) {
          if (itemId == this.serviceCataloDetails[i]['stageID']) {
            this.moduleList.push({
              id: this.serviceCataloDetails[i]['modules'][j].moduleId, itemName: this.serviceCataloDetails[i]['modules'][j].moduleName
            });
          }
        }
      }
    }
    let cloneModuleList = [...this.moduleList];
    this.moduleList = [];
    this.moduleList = cloneModuleList;
    return this.moduleList;
  }



  subModuleListItems(itemId) {
    for (let i = 0; i <= this.serviceCataloDetails.length - 1; i++) {
      if (this.serviceCataloDetails[i]['modules'].length > 0) {
        for (let j = 0; j <= this.serviceCataloDetails[i]['modules'].length - 1; j++) {
          if (this.serviceCataloDetails[i]['modules'][j]['submodules'].length > 0) {
            for (let k = 0; k <= this.serviceCataloDetails[i]['modules'][j]['submodules'].length - 1; k++) {
              if (itemId == this.serviceCataloDetails[i]['modules'][j]['moduleId']) {
                this.subModuleList.push({
                  id: this.serviceCataloDetails[i]['modules'][j]['submodules'][k].subModuleId, itemName: this.serviceCataloDetails[i]['modules'][j]['submodules'][k].subModuleName, category: this.serviceCataloDetails[i]['modules'][j].moduleName
                });
              }
            }
          }
        }
      }
    }

    let cloneSubModuleList = [...this.subModuleList];
    this.subModuleList = [];
    this.subModuleList = cloneSubModuleList;
    return this.subModuleList;
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }


  submitForm(formData) {
    this.validateAllFormFields(this.projectTypeRegulationForm);
    this.isSubmitted = true;
    if (this.projectTypeRegulationForm.valid) {
      $('.spinner').show();

      var filteredSubModule = formData["SubModule"].filter(function (el) {
        return el != null;
      });

      var data = [];
      this.isNumber(formData["ProjectTypeDescription"]);
      if (filteredSubModule.length > 0 && this.descriptionResult == false) {
        this.disabled = true;
        for (let k = 0; k <= filteredSubModule.length - 1; k++) {
          data.push({
            UserId: this.user_id,
            StageId: formData["Stage"][0]["id"],
            SubModuleId: filteredSubModule[k]['id'] * 1,
            ProjectType: formData["ProjectType"],
            flag: 0,
            Active: (formData["active"] == "true"),
            ProjectTypeDescription: formData["ProjectTypeDescription"]
          });
        }
      }


      if (data.length > 0) {
        this.ProjectTypeRegulation.postRequest('InsertProjectType', data).subscribe((response) => {
          console.log(response)
          if (response.status == 1) {
            $('.close').trigger('click');
            this.Close();
            this.GetProjectTypeRegulationList();
            this.successmessage("Project Type created successfully");
            this.disabled = false;
            this.isSubmitted = false;
          }
          else {
            if (response["status"] == 2) {
        $('.spinner').hide();

              this.Errormessage('Project type already exists');
              this.disabled = false;
            } else if (response["status"] == 3) {
        $('.spinner').hide();

              this.Errormessage(response['message']);
              this.disabled = false;
            }
          }
        },err =>{
        $('.spinner').hide();

        });
      }
    }
  }




  editProjectTypeRegulation(projectTypeRegulation: any) {
$('.spinner').show();
    this.ProjectTypeRegulation.getRequest('GetProjectTypeListById/' + projectTypeRegulation.projectTypeId + '').subscribe(EditProjectTypeDetails => {

      this.editData = EditProjectTypeDetails;
      let modules = {};
      var resultData = [];
      var sub_module_array = {};
      var datsIds2 = [];


      if (this.editData != null) {
        this.editData.forEach(resource => {

          resultData['projectTypeId'] = resource.projectTypeId;
          resultData['projectType'] = resource.projectType;
          resultData['projectTypeDescription'] = resource.projectTypeDescription;
          resultData['stageId'] = resource.stageId;
          resultData['stageName'] = resource.stageName;

          if (!modules.hasOwnProperty(resource.moduleId)) {
            modules[resource.moduleId] = {};
            modules[resource.moduleId]['moduleId'] = resource.moduleId;
            modules[resource.moduleId]['moduleName'] = resource.moduleName;
            modules[resource.moduleId]['subModules'] = {};
          }


          if (!modules[resource.moduleId]['subModules'].hasOwnProperty(resource.subModuleId)) {
            modules[resource.moduleId]['subModules'][resource.subModuleId] = {};
            modules[resource.moduleId]['subModules'][resource.subModuleId]['subModuleId'] = resource.subModuleId;
            modules[resource.moduleId]['subModules'][resource.subModuleId]['subModuleName'] = resource.subModuleName;
            sub_module_array[resource.subModuleId] = {};
          }

        });

        resultData['modules'] = modules;
        this.result = resultData;
      }



      this.projectType = resultData["projectType"];
      this.projectTypeDescription = resultData["projectTypeDescription"];


      var modulesDropdown = [];
      var subModuleDropDown = [];
      Object.keys(resultData['modules']).forEach(function (moduleKey) {
        datsIds2.push(resultData['modules'][moduleKey]['moduleId']);
        modulesDropdown.push({
          id: resultData['modules'][moduleKey]['moduleId'], itemName: resultData['modules'][moduleKey]['moduleName'],
        });

        Object.keys(resultData['modules'][moduleKey]['subModules']).forEach(function (subModuleKey) {
          subModuleDropDown.push({
            id: resultData['modules'][moduleKey]['subModules'][subModuleKey]['subModuleId'], itemName: resultData['modules'][moduleKey]['subModules'][subModuleKey]['subModuleName'], category: resultData['modules'][moduleKey]['moduleName']
          });
        });
      });

      this.stageSelectedItems = [
        { id: resultData['stageId'], itemName: resultData['stageName'] }
      ];

      this.subModuleLoop(datsIds2);
      this.moduleListItems(resultData['stageId']);
      this.moduleSelectedItems = modulesDropdown;
      this.subModuleSelectedItems = subModuleDropDown;
      this.dupSubModuleSelectedItems = this.subModuleSelectedItems;
      this.selectedValue = String(projectTypeRegulation["active"]);
      this.uploadData = EditProjectTypeDetails;
      $('.spinner').hide();
    }, err =>{
      $('.spinner').hide();
    });
  }


  updateForm(formData) {
    this.isSubmitted = true;
    this.validateAllFormFields(this.projectTypeRegulationForm);
    if (this.projectTypeRegulationForm.valid) {
      $('.spinner').show();

      var filteredSubModule = formData["SubModule"].filter(function (el) {
        return el != null;
      });

      var data = [];
      this.isNumber(formData["ProjectTypeDescription"]);
      if (filteredSubModule.length > 0 && this.descriptionResult == false) {
        this.disabled = true;
        for (let k = 0; k <= filteredSubModule.length - 1; k++) {
          data.push({
            ProjectTypeId: this.result['projectTypeId'],
            UserId: this.user_id,
            StageId: formData["Stage"][0]["id"],
            SubModuleId: filteredSubModule[k]['id'] * 1,
            ProjectType: formData["ProjectType"],
            flag: 1,
            Active: (formData["active"] == "true"),
            ProjectTypeDescription: formData["ProjectTypeDescription"]
          });
        }
      }


      if (data.length > 0) {
        this.ProjectTypeRegulation.postRequest('UpdateProjectType', data).subscribe((response) => {
          if (response.status == 1) {
            $('.close').trigger('click');
            this.Close();
            this.GetProjectTypeRegulationList();
            this.successmessage("Project type regulation updated successfully");
            this.disabled = false;
            this.isSubmitted = false;
          }
          else {
        $('.spinner').hide();

            if (response["status"] == 2) {
              this.Errormessage('Project type already exists');
              this.disabled = false;
            } else if (response["status"] == 3) {
              this.disabled = false;
              this.Errormessage(response['message']);
            }
          }
        }, err =>{
        $('.spinner').hide();

        });
      }
    }
  }


  deleteProjectType(projectType) {
    this.currentProjectTypeId = projectType.projectTypeId;
    this.deleteProjectTypeName = projectType.projectType;
  }

  deleteProjectTypeConfirm() {
    $('.spinner').show();

    this.ProjectTypeRegulation.getRequest('DeleteProjectTypeRegulation/' + this.currentProjectTypeId).subscribe((response) => {
      if (response.status == 1) {
        this.GetProjectTypeRegulationList();
        $('.modal-close').trigger('click');
        this.successmessage("Project type deleted successfully");
      }
      else {
        $('.spinner').hide();

        if (response["status"] == 2) {
          this.Errormessage('Project type already exists');
        }
      }
    },err =>{
      $('.spinner').hide();

    });

  }


  isNumber(value: string | number): boolean {
    var result = ((value != null) &&
      (value !== '') &&
      !isNaN(Number(value.toString())));
    this.descriptionResult = result;
    return result;
  }


  public get getFields() {
    return this.projectTypeRegulationForm.controls;
  }

  Open() {
    this.selectedValue = 'true';
  }

  Close() {
    this.projectTypeRegulationForm.reset();
    this.subModuleList = [];
    this.isSubmitted = false;
  }

  successmessage(message) {
    this.messageService.add({ severity: 'success', summary: 'Success', detail: message });
  }

  Errormessage(errorsmessage) {
    this.messageService.add({ severity: 'error', summary: 'Error', detail: errorsmessage });
  }

}
