import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from "@angular/forms";
import { ServiceCatalogService } from 'src/app/services/service-catalog.service';
import { formValidators } from 'src/app/helper/formValidators';
import { SharedvalueService } from 'src/app/services/sharedvalue.service';
declare var $: any;
@Component({
  selector: 'app-servicecatalog',
  templateUrl: './servicecatalog.component.html',
  providers: [ServiceCatalogService, ToastModule, MessageService, FormBuilder],
  styleUrls: ['./servicecatalog.component.css']
})
export class ServicecatalogComponent implements OnInit {
  cols: any[];
  accord: any[];
  serviceCataloDetails: any;
  data: any = [];
  serviceCataloDetailsForm: FormGroup;
  menuName: any;
  submenuName: any;
  privilegeName: any;
  uploadData: any;
  uploadedsubmoduleData: any;
  uploadprivilegeData: any;
  showmenu: boolean;
  moduleDropdown: any[];
  submoduleDropdown: any;
  selectedValue: any;
  disadble: boolean;
  selectedmodule: any;
  selectedsubmodule: any;
  selectedmoduleNameList: any;
  selectedmoduleNameLists: any;
  selectedprivilegeList: any;
  selectedsubmoduleList: any;
  selectedsubmoduleLists: any;
  selectedsubmoduleNameList: any;
  stageName: any;
  showtable: boolean;
  disadbleSubmit: boolean;
  serviceCatalogInputJson: any = [];
  showBackButton: any;
  stageId: any;
  moduleId: any;
  submoduleId: any;
  showEditPopup: boolean;
  ShowErrormessage: boolean;
  addDisable: boolean;
  indexs: number = 0;
  visibleIndex: number = -1;
  showerror: boolean;
  menuNameshowerror: boolean;
  moduleDropdownshowerror: boolean;
  oldButton: boolean;
  newButton: boolean;
  tabindex: any;
  Old: any;
  showSideBar = true;
  private inputdataarray: Array<any> = [];

  constructor(
    private http: HttpClient, 
    private toast: ToastModule, 
    private message: MessageService, 
    private formBuilder: FormBuilder, 
    private serviceCatalog: ServiceCatalogService,
    private shrService: SharedvalueService) {

    this.serviceCataloDetailsForm = formBuilder.group({
      StageName: ["", [Validators.required, formValidators.alphabetical, formValidators.noWhitespace, Validators.maxLength(50)]],
      MenuName: ["", [Validators.required, formValidators.alphabetical, formValidators.noWhitespace, Validators.maxLength(50)]],
      subModuleName: ["", [formValidators.alphabeticalComma]],
      PrivilegeName: ["", [Validators.required, formValidators.alphabeticalComma]],
      active: "",
      rememberMeFlag: [false],
      moduleDropdoenList: ["", [Validators.required]],
      submoduleDropdownList: ["", []],
      "moduleinputfield": this.formBuilder.array([]),
      "submoduleinputfield": this.formBuilder.array([])
    });


  }

  ngOnInit() {
    $('.spinner').show();
    this.shrService.getSideBarDetail().subscribe(resp => { this.showSideBar = resp });
    //debugger
    //this.showEditPopup = false;    
    this.disadble = false;
    this.showtable = false;
    this.showerror = true;
    this.newButton = true;
    this.oldButton = false;
    this.menuNameshowerror = true;
    // this.serviceCataloDetailsForm.controls['MenuName'].disable();
    // this.serviceCataloDetailsForm.controls['subModuleName'].disable();
    // this.serviceCataloDetailsForm.controls['PrivilegeName'].disable();

    this.cols = [
      {
        field: 'dataextraction', header: 'DATA Extraction',
        accordian: [{ accfield: 'Job', accheader: 'JOB', privilage: [{ name: 'Job Onboard', pre: [{ prename: 'Job Onboard' }, { prename: 'Bot Onboard' }, { prename: 'Google Bot' }, { prename: 'Social Media Bot' }] }, { name: 'Bot Onboard' }] }, { accfield: 'FederatedSearch', accheader: 'FEDERATED SEARCH' }]
      },
      { field: 'datapreparation', header: 'DATA PREPARATION' },
      { field: 'dataenrichment', header: 'DATA ENRICHMENT' }
      // { field: 'active', header: 'Status' }
    ];

    this.GetServiceCatalogDetails();
    this.accord = [
      { field: 'Job', header: 'JOB' },
      { field: 'FederatedSearch', header: 'FEDERATED SEARCH' },
      { field: 'Bot', header: 'BOT' }
    ];

    this.WizardNavBar();

  }


  GetServiceCatalogDetails() {
    this.serviceCatalog.getRequest('GetServiceCatalogDetails').subscribe((ServiceCatalog) => {
      //debugger

      this.serviceCataloDetails = ServiceCatalog;
      this.showmenu = true;
      $('.spinner').hide();
    },err =>{
      $('.spinner').hide();
    });
  }


  public get getFields() {
    return this.serviceCataloDetailsForm.controls;
  }

  validateAllFormFields(formGroup: FormGroup) {

    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  EditModule(module: any) {
    this.serviceCatalog.getRequest('GetServiceCatalogDetailsById/' + module.moduleId + '').subscribe((ServiceCatalog) => {
      //debugger
      this.serviceCataloDetailsForm.controls['MenuName'].enable();
      this.menuName = module.moduleName;
      this.selectedValue = String(ServiceCatalog.active);
      this.uploadData = module;
      //this.showEditPopup = true;
    });
  }

  EditSubModule(submoduleData: any) {
    //debugger
    $('.spinner').show();
    this.serviceCatalog.getRequest('GetServiceCatalogDetailsById/' + submoduleData.subModuleId + '').subscribe((ServiceCatalog) => {
      //debugger
      $('.spinner').hide();
      this.serviceCataloDetailsForm.controls['subModuleName'].enable();
      this.submenuName = submoduleData.subModuleName;
      this.selectedValue = String(ServiceCatalog.active);
      this.uploadedsubmoduleData = submoduleData;
    },err =>{
      $('.spinner').hide();
    });

  }

  EditPrivilege(privilegeData: any) {
    //debugger
    $('.spinner').show();
    this.serviceCatalog.getRequest('GetPrivilegeServiceCatalogDetailsById/' + privilegeData.privilegeId + '').subscribe((ServiceCatalog) => {
      //debugger
      this.privilegeName = privilegeData.privilege_Name;
      this.selectedValue = String(ServiceCatalog.active);
      this.uploadprivilegeData = privilegeData;
      $('.spinner').hide();
    },err =>{
      $('.spinner').hide();
    });

  }


  tableFormation() {
    //debugger
    //this.serviceCatalogInputJson = [];
    var arrString = this.selectedprivilegeList[0].privilegeName.split(',');

    var extmoduledata = this.serviceCatalogInputJson.filter(item => item.modulename == this.selectedmoduleNameList);

    var extdata = extmoduledata.filter(item => item.submodulename == this.selectedsubmoduleList)

    for (let i = 0; i <= arrString.length - 1; i++) {
      if (extdata.filter(item => item.Privilege_Name == arrString[i]).length == 0) {
        this.serviceCatalogInputJson.push({ stagename: this.stageName, modulename: this.selectedmoduleNameList, submodulename: this.selectedsubmoduleList, Privilege_Name: arrString[i] })
      }
      // if(this.selectedsubmoduleList == undefined){
      //   this.serviceCatalogInputJson.push({stagename:this.stageName,modulename:this.selectedmoduleNameList,submodulename:this.selectedsubmoduleList,Privilege_Name:arrString[i]})
      // }
    }
    this.showtable = true;
    this.showerror = false;
    this.serviceCataloDetailsForm.controls['PrivilegeName'].setValue(null);

  }

  submoduleinputfield: Array<any> = [];
  submoduleinputnewAttribute: any = {};

  addsubmoduleFieldValue(privilegeName) {
    //debugger
    this.validateAllFormFields(this.serviceCataloDetailsForm);
    if (this.serviceCataloDetailsForm.valid) {
      this.selectedprivilegeList = [{
        privilegeName: this.serviceCataloDetailsForm.value.PrivilegeName
      }];
      this.tableFormation();
    }
    else {
      this.showerror = true;
      this.moduleDropdownshowerror = true;
    }


    // if(this.selectedprivilegeList[0].privilegeName != undefined){
    //   this.showtable = true;
    // }


  }

  deletesubmoduleFieldValue(index: number) {
    const control = <FormArray>this.serviceCataloDetailsForm.controls['submoduleinputfield'];
    control.removeAt(index);
  }

  createsubmoduleinputItem(): FormGroup {
    return this.formBuilder.group({
      PrivilegeNames: new FormControl("")
    });

  }

  moduleinputfield: Array<any> = [];
  moduleinputnewAttribute: any = {};

  addmoduleFieldValue() {
    //debugger
    const control = <FormArray>this.serviceCataloDetailsForm.controls['moduleinputfield'];
    control.push(this.createmoduleinputItem());
    //console.log(this.botform);

    this.moduleinputfield.push(this.moduleinputnewAttribute)
    this.moduleinputnewAttribute = {};
  }

  createmoduleinputItem(): FormGroup {
    return this.formBuilder.group({
      ModuleNames: new FormControl(""),
      SubModuleName: new FormControl("")
    });
  }

  deletemoduleFieldValue(index: number) {
    this.submoduleDropdown = [];
    const control = <FormArray>this.serviceCataloDetailsForm.controls['moduleinputfield'];
    control.removeAt(index);
  }

  cancelFieldChanges() {
    const control = <FormArray>this.serviceCataloDetailsForm.controls['moduleinputfield'];
    control.controls = [];
  }

  AddModule(item: any) {
    //debugger
    this.newButton = false;
    this.oldButton = true;
    this.Old = 'NewModule';
    this.showtable = false;
    this.moduleDropdown = [];
    this.submoduleDropdown = [];
    this.serviceCataloDetailsForm.controls['StageName'].enable();
    this.serviceCataloDetailsForm.controls['MenuName'].enable();
    this.serviceCataloDetailsForm.controls['subModuleName'].enable();

    this.serviceCataloDetailsForm.controls['StageName'].disable();
    //this.serviceCataloDetailsForm.controls['MenuName'].disable();

    this.serviceCataloDetailsForm.controls['StageName'].setValue(item.stageName);
    this.stageId = item.stageID;
    $(".tab2").trigger('click');
  }

  AddSubModule(serviceCataloDetails: any, items: any) {
    //debugger
    this.newButton = false;
    this.oldButton = true;
    this.Old = 'NewSubModule'
    this.moduleDropdown = [];
    this.submoduleDropdown = [];
    this.showtable = false;
    this.serviceCataloDetailsForm.controls['StageName'].enable();
    this.serviceCataloDetailsForm.controls['MenuName'].enable();
    this.serviceCataloDetailsForm.controls['subModuleName'].enable();

    this.serviceCataloDetailsForm.controls['StageName'].disable();
    this.serviceCataloDetailsForm.controls['MenuName'].disable();

    this.serviceCataloDetailsForm.controls['StageName'].setValue(serviceCataloDetails.stageName);
    this.stageId = serviceCataloDetails.stageID;
    this.serviceCataloDetailsForm.controls['MenuName'].setValue(items.moduleName);
    this.moduleId = items.moduleId;

    $(".tab2").trigger('click');
  }
  AddPrivilege(serviceCataloDetails: any, items: any, data: any) {
    //debugger
    this.newButton = false;
    this.oldButton = true;
    this.showtable = false;
    this.addDisable = true;
    this.Old = 'NewPrivilege'
    this.serviceCataloDetailsForm.controls['StageName'].enable();
    this.serviceCataloDetailsForm.controls['MenuName'].enable();
    this.serviceCataloDetailsForm.controls['subModuleName'].enable();

    this.serviceCataloDetailsForm.controls['StageName'].disable();
    this.serviceCataloDetailsForm.controls['MenuName'].disable();
    this.serviceCataloDetailsForm.controls['subModuleName'].disable();

    this.serviceCataloDetailsForm.controls['StageName'].setValue(serviceCataloDetails.stageName);
    this.stageId = serviceCataloDetails.stageID;
    this.serviceCataloDetailsForm.controls['MenuName'].setValue(items.moduleName);
    this.moduleId = items.moduleId;
    this.serviceCataloDetailsForm.controls['subModuleName'].setValue(data.subModuleName);
    this.submoduleId = data.subModuleId
    this.moduleDropdown = [{
      modulename: this.menuName,
      submodulename: this.submenuName
    }];
    // this.privilegeName = privilegeData.privilege_Name;
    $(".tab3").trigger('click');
  }

  InsertForm() {
    //debugger
    $('.spinner').show();
    this.disadble = true;
    //alert(JSON.stringify(this.serviceCatalogInputJson));
    if (this.serviceCatalogInputJson.length != 0) {
      this.serviceCatalog.postRequest('InsertServiceCatalog', this.serviceCatalogInputJson).subscribe((response) => {
        //debugger
        if (response.status == 1) {
          $(".tab1").trigger('click');
          $('.close').trigger('click');
          this.Close();
          this.cancelFieldChanges();
          this.showtable = false;
          this.GetServiceCatalogDetails();
          this.successmessage("ServiceCatalog Created Successfully");
          this.disadble = false;
        }
        else {
          //debugger
          $('.spinner').hide();
          this.disadble = false;
          this.Errormessage(response["message"]);
        }
      },err =>{
        $('.spinner').hide();
      });
    } else {
      $('.spinner').hide();
      this.disadble = false;
      this.Errormessage("Error");
    }

  }


  UpdateForm(formData) {
    //debugger
   
    this.disadble = true;
    this.validateAllFormFields(this.serviceCataloDetailsForm);
    if (this.serviceCataloDetailsForm.value.MenuName != '' && this.serviceCataloDetailsForm.value.MenuName != null) {
      $('.spinner').show();
      if (this.serviceCataloDetailsForm.controls['MenuName'].valid != false) {
        var data = {
          Id: this.uploadData["moduleId"],
          ProcessId: 1,
          Name: formData["MenuName"],
          Active: (formData["active"] == "true")
        }
        //alert(JSON.stringify(data));
        this.serviceCatalog.postRequest('UpdateServiceCatalog', data).subscribe((response) => {
          //debugger
          if (response.status == 1) {
            $('.close').trigger('click');
            this.Close();
            this.GetServiceCatalogDetails();
            this.successmessage("Module Updated Successfully");
            this.disadble = false;
          }
          else {
            //debugger
            $('.spinner').hide();
            this.disadble = false;
            this.Errormessage(response["message"]);
          }
        },err =>{
          $('.spinner').hide();
        });
      } else {
        $('.spinner').hide();
        this.disadble = false;
      }
    }
    else if (this.serviceCataloDetailsForm.value.subModuleName != '' && this.serviceCataloDetailsForm.value.subModuleName != null) {
      $('.spinner').show();
      if (this.serviceCataloDetailsForm.controls['subModuleName'].valid != false) {
        var data = {
          Id: this.uploadedsubmoduleData["subModuleId"],
          ProcessId: 1,
          Name: formData["subModuleName"],
          Active: (formData["active"] == "true")
        }
        //alert(JSON.stringify(data));
        this.serviceCatalog.postRequest('UpdateServiceCatalog', data).subscribe((response) => {
          //debugger
          if (response.status == 1) {
            $('.close').trigger('click');
            this.Close();
            this.GetServiceCatalogDetails();
            this.successmessage("SubModule Updated Successfully");
            this.disadble = false;
          }
          else {
            //debugger
            this.disadble = false;
            this.Errormessage(response["message"]);
            $('.spinner').hide();
          }
        },err =>{
          $('.spinner').hide();
        });
      } else {
        $('.spinner').hide();
        this.disadble = false;
      }
    }
    else if (this.serviceCataloDetailsForm.value.PrivilegeName != '' && this.serviceCataloDetailsForm.value.PrivilegeName != null) {
      $('.spinner').show();
      if (this.serviceCataloDetailsForm.controls['PrivilegeName'].valid != false) {
        var data = {
          Id: this.uploadprivilegeData["privilegeId"],
          ProcessId: 2,
          Name: formData["PrivilegeName"],
          Active: (formData["active"] == "true")
        }
        //alert(JSON.stringify(data));
        this.serviceCatalog.postRequest('UpdateServiceCatalog', data).subscribe((response) => {
          //debugger
          if (response.status == 1) {
            $('.close').trigger('click');
            this.Close();
            this.GetServiceCatalogDetails();
            this.successmessage("Privilege Updated Successfully");
            this.disadble = false;
          }
          else {
            //debugger
           $('.spinner').hide();
            this.disadble = false;
            this.Errormessage(response["message"]);
          }
        });
      } else {
        $('.spinner').hide();
        this.disadble = false;
      }
    }
    else {
      this.disadble = false;
    }

  }

  OpenModule() {
    this.newButton = true;
    this.oldButton = false;
    this.serviceCataloDetailsForm.controls['StageName'].enable();
    this.serviceCataloDetailsForm.controls['MenuName'].enable();
    this.serviceCataloDetailsForm.controls['subModuleName'].enable();
    this.showtable = false;
  }
  Close() {
    this.serviceCataloDetailsForm.reset();
    $(".tab1").trigger('click');
    this.cancelFieldChanges();
    this.serviceCatalogInputJson = [];
    this.addDisable = false;
    this.showerror = true;
    // this.serviceCatalogInputJson.reset();
    // this.serviceCatalogInputJson.reset();
    // this.selectedmoduleNameList.reset();
    // this.selectedsubmoduleList.reset();
  }

  NextTabNavBars(ID) {
    //alert('NextTabNavBars');
    this.serviceCataloDetailsForm.controls['StageName'].enable();
    this.serviceCataloDetailsForm.controls['MenuName'].enable();
    this.serviceCataloDetailsForm.controls['subModuleName'].enable();
    this.validateAllFormFields(this.serviceCataloDetailsForm);
    if (ID != ".tab3") {
      if (this.serviceCataloDetailsForm.controls['StageName'].valid == false) {

      } else {
        this.menuNameshowerror = false;
        this.stageName = this.serviceCataloDetailsForm.controls['StageName'].value;
        // this.moduleDropdown = [{
        //   modulename: this.serviceCataloDetailsForm.value.MenuName,
        //   submodulename: this.serviceCataloDetailsForm.value.subModuleName
        // }];
        if (this.serviceCataloDetailsForm.controls['MenuName'].value != undefined) {
          this.moduleDropdown = [{
            modulename: this.serviceCataloDetailsForm.controls['MenuName'].value,
            submodulename: this.serviceCataloDetailsForm.controls['subModuleName'].value
          }];

          //this.selectedmoduleNameList = this.moduleDropdown; 

          this.serviceCataloDetailsForm.value.moduleinputfield.forEach(data => {
            //debugger
            if (data.ModuleNames != "") {
              this.moduleDropdown.push({ modulename: data.ModuleNames, submodulename: data.SubModuleName });
            }
            else {
              this.ShowErrormessage = true;
            }

          });
        }
        if (this.Old == 'NewModule') {
          this.serviceCataloDetailsForm.controls['StageName'].disable();
        }
        else if (this.Old == 'NewPrivilege') {
          this.serviceCataloDetailsForm.controls['StageName'].disable();
          this.serviceCataloDetailsForm.controls['MenuName'].disable();
          this.serviceCataloDetailsForm.controls['subModuleName'].disable();
        }
        else {
          this.serviceCataloDetailsForm.controls['StageName'].disable();
          this.serviceCataloDetailsForm.controls['MenuName'].disable();
        }
        $(ID).trigger('click');
      }
    } else {
      if (this.serviceCataloDetailsForm.controls['MenuName'].valid == false || this.serviceCataloDetailsForm.controls['subModuleName'].valid == false) {
        this.menuNameshowerror = true;
        if (this.Old == 'NewModule') {
          this.serviceCataloDetailsForm.controls['StageName'].disable();
        }
        else if (this.Old == 'NewPrivilege') {
          this.serviceCataloDetailsForm.controls['StageName'].disable();
          this.serviceCataloDetailsForm.controls['MenuName'].disable();
          this.serviceCataloDetailsForm.controls['subModuleName'].disable();
        }
        else {
          this.serviceCataloDetailsForm.controls['StageName'].disable();
          this.serviceCataloDetailsForm.controls['MenuName'].disable();
        }
      } else {
        this.serviceCataloDetailsForm.controls['StageName'].disable();
        this.serviceCataloDetailsForm.controls['MenuName'].disable();
        this.moduleDropdownshowerror = false;
        this.showerror = false;
        this.stageName = this.serviceCataloDetailsForm.controls['StageName'].value;
        // this.moduleDropdown = [{
        //   modulename: this.serviceCataloDetailsForm.value.MenuName,
        //   submodulename: this.serviceCataloDetailsForm.value.subModuleName
        // }];
        if (this.serviceCataloDetailsForm.controls['MenuName'].value != undefined) {
          this.moduleDropdown = [{
            modulename: this.serviceCataloDetailsForm.controls['MenuName'].value,
            submodulename: this.serviceCataloDetailsForm.controls['subModuleName'].value
          }];

          //this.selectedmoduleNameList = this.moduleDropdown; 

          this.serviceCataloDetailsForm.value.moduleinputfield.forEach(data => {
            //debugger
            if (data.ModuleNames != "") {
              this.moduleDropdown.push({ modulename: data.ModuleNames, submodulename: data.SubModuleName });
            }
            else {
              this.ShowErrormessage = true;
            }

          });
        }

        $(ID).trigger('click');
      }
    }

    //this.serviceCataloDetailsForm.value.moduleinputfield[0].ModuleNames

    // for(let i = 0; i <= this.serviceCataloDetailsForm.value.moduleinputfield.length - 1; i++){
    //   if(this.serviceCataloDetailsForm.value.moduleinputfield[1].ModuleNames == ""){
    //     this.ShowErrormessage = true;
    //   }
    //   else{
    //     //this.ShowErrormessage = false;
    //   }
    // } 

  }


  NextTabNavBar(ID) {
    //alert('NextTabNavBar')
    //this.serviceCataloDetailsForm.controls['MenuName'].enable();
    this.validateAllFormFields(this.serviceCataloDetailsForm);
    if (ID != ".tab3") {
      if (this.serviceCataloDetailsForm.controls['StageName'].valid == false) {

      } else {
        this.menuNameshowerror = false;
        this.stageName = this.serviceCataloDetailsForm.controls['StageName'].value;
        // this.moduleDropdown = [{
        //   modulename: this.serviceCataloDetailsForm.value.MenuName,
        //   submodulename: this.serviceCataloDetailsForm.value.subModuleName
        // }];
        if (this.serviceCataloDetailsForm.controls['MenuName'].value != undefined) {
          this.moduleDropdown = [{
            modulename: this.serviceCataloDetailsForm.controls['MenuName'].value,
            submodulename: this.serviceCataloDetailsForm.controls['subModuleName'].value
          }];

          //this.selectedmoduleNameList = this.moduleDropdown; 

          this.serviceCataloDetailsForm.value.moduleinputfield.forEach(data => {
            //debugger
            if (data.ModuleNames != "") {
              this.moduleDropdown.push({ modulename: data.ModuleNames, submodulename: data.SubModuleName });
            }
            else {
              this.ShowErrormessage = true;
            }

          });
        }

        $(ID).trigger('click');
      }
    } else {
      if (this.serviceCataloDetailsForm.controls['MenuName'].valid == false || this.serviceCataloDetailsForm.controls['subModuleName'].valid == false) {
        this.menuNameshowerror = true;
        //this.serviceCataloDetailsForm.controls['MenuName'].disable();
      } else {
        //this.serviceCataloDetailsForm.controls['MenuName'].disable();
        this.moduleDropdownshowerror = false;
        this.showerror = false;
        this.stageName = this.serviceCataloDetailsForm.controls['StageName'].value;
        // this.moduleDropdown = [{
        //   modulename: this.serviceCataloDetailsForm.value.MenuName,
        //   submodulename: this.serviceCataloDetailsForm.value.subModuleName
        // }];
        if (this.serviceCataloDetailsForm.controls['MenuName'].value != undefined) {
          this.moduleDropdown = [{
            modulename: this.serviceCataloDetailsForm.controls['MenuName'].value,
            submodulename: this.serviceCataloDetailsForm.controls['subModuleName'].value
          }];

          //this.selectedmoduleNameList = this.moduleDropdown; 

          this.serviceCataloDetailsForm.value.moduleinputfield.forEach(data => {
            //debugger
            if (data.ModuleNames != "") {
              this.moduleDropdown.push({ modulename: data.ModuleNames, submodulename: data.SubModuleName });
            }
            else {
              this.ShowErrormessage = true;
            }

          });
        }

        $(ID).trigger('click');
      }
    }

    //this.serviceCataloDetailsForm.value.moduleinputfield[0].ModuleNames

    // for(let i = 0; i <= this.serviceCataloDetailsForm.value.moduleinputfield.length - 1; i++){
    //   if(this.serviceCataloDetailsForm.value.moduleinputfield[1].ModuleNames == ""){
    //     this.ShowErrormessage = true;
    //   }
    //   else{
    //     //this.ShowErrormessage = false;
    //   }
    // } 


  }

  PreviousTabNavBar(ID) {
    $(ID).trigger('click');
  }


  ScrollNext() {
    //alert()

    //debugger
    $(document).ready(function () {
      $('.ui-tabview-top .ui-tabview-nav .ui-state-active').attr("style", "display: none !important");
    });


    //     //debugger
    //   // duration of scroll animation
    // var scrollDuration = 900;
    // // paddles
    // var leftPaddle = document.getElementsByClassName('left-paddle');
    // var rightPaddle = document.getElementsByClassName('right-paddle');
    // // get items dimensions
    // var itemsLength = $('.ui-corner-top').length;
    // var itemSize = $('.ui-corner-top').outerWidth(true);
    // // get some relevant size for the paddle triggering point
    // var paddleMargin = 20;

    // // get wrapper width
    // var getMenuWrapperSize = function() {
    // 	return $('.ui-tabview-top').outerWidth();
    // }
    // var menuWrapperSize = getMenuWrapperSize();
    // // the wrapper is responsive
    // $(window).on('resize', function() {
    // 	menuWrapperSize = getMenuWrapperSize();
    // });
    // // size of the visible part of the menu is equal as the wrapper size 
    // var menuVisibleSize = menuWrapperSize;

    // // get total width of all menu items
    // var getMenuSize = function() {
    // 	return itemsLength * itemSize;
    // };
    // var menuSize = getMenuSize();
    // // get how much of menu is invisible
    // var menuInvisibleSize = menuSize - menuWrapperSize;

    // // get how much have we scrolled to the left
    // var getMenuPosition = function() {
    // 	return $('.ui-tabview-nav').scrollLeft();
    // };

    // // finally, what happens when we are actually scrolling the menu
    // $('.ui-tabview-nav').on('scroll', function() {

    // 	// get how much of menu is invisible
    // 	menuInvisibleSize = menuSize - menuWrapperSize;
    // 	// get how much have we scrolled so far
    // 	var menuPosition = getMenuPosition();

    // 	var menuEndOffset = menuInvisibleSize - paddleMargin;

    // 	// show & hide the paddles 
    // 	// depending on scroll position
    // 	if (menuPosition <= paddleMargin) {
    // 		$(leftPaddle).addClass('hidden');
    // 		$(rightPaddle).removeClass('hidden');
    // 	} else if (menuPosition < menuEndOffset) {
    // 		// show both paddles in the middle
    // 		$(leftPaddle).removeClass('hidden');
    // 		$(rightPaddle).removeClass('hidden');
    // 	} else if (menuPosition >= menuEndOffset) {
    // 		$(leftPaddle).removeClass('hidden');
    // 		$(rightPaddle).addClass('hidden');
    // }


    // });

    // // scroll to left
    // $(rightPaddle).on('click', function() {
    // 	$('.ui-tabview-nav').animate( { scrollLeft: menuInvisibleSize}, scrollDuration);
    // });

    // // scroll to right
    // $(leftPaddle).on('click', function() {
    // 	$('.ui-tabview-nav').animate( { scrollLeft: '0' }, scrollDuration);
    // });

  }


  WizardNavBar() {
    $(document).ready(function () {
      $(".tab1").click(function () {
        $(".tab2-section").hide();
        $(".tab3-section").hide();
        $(".tab2a-section").hide();
        $(".tab1-section").show();
        $(".tab2 , .tab3").removeClass("active");
      });

      $(".tab2").click(function () {
        $(".tab1-section").hide();
        $(".tab3-section").hide();
        $(".tab2a-section").hide();
        $(".tab2-section").show();
        $(".tab2").addClass("active");
        $(".tab3").removeClass("active");
      });

      $(".tab3").click(function () {
        $(".tab1-section").hide();
        $(".tab2-section").hide();
        $(".tab2a-section").hide();
        $(".tab3-section").show();
        $(".tab2 , .tab3").addClass("active");
      });

    });

  }
  getselectedmodule() {
    //debugger
    this.submoduleDropdown = [];

    if (this.selectedmodule.submodulename != undefined) {

      var arrString = this.selectedmodule.submodulename.split(',');

      for (let i = 0; i <= arrString.length - 1; i++) {
        this.submoduleDropdown.push({ submodulename: arrString[i], modulename: this.selectedmodule.modulename });
      }
    }

    //this.selectedmoduleNameLists =  this.submoduleDropdown;

    this.selectedmoduleNameList = this.selectedmodule.modulename;
  }
  getselectedsubmodule() {
    //debugger
    this.selectedsubmoduleList = this.selectedsubmodule.submodulename;
    this.selectedsubmoduleLists = this.selectedsubmodule;
    //alert(JSON.stringify(this.selectedsubmodule))
  }
  successmessage(message) {
    this.message.add({ severity: 'success', summary: 'Success Message', detail: message });
  }

  Errormessage(errorsmessage) {
    this.message.add({ severity: 'error', summary: '', detail: errorsmessage });
  }
  clear() {
    this.message.clear();
  }

  openNext() {
    this.indexs = (this.indexs === this.serviceCataloDetails.length - 1) ? 0 : this.indexs + 1;
  }

  openPrev() {
    this.indexs = (this.indexs === 0) ? this.serviceCataloDetails.length - 1 : this.indexs - 1;
  }

  handleChange(e) {
    //debugger
    this.indexs = e.index;
  }

}
