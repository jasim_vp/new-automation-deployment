import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchf',
  pure: false
})
export class SearchPipe implements PipeTransform {

  transform(items: any[], filterdata: string): any[] {
    if (!items) { return []; }
    if (!filterdata) { return items; }
    filterdata = filterdata.toString().toLowerCase();
    return items.filter(it => {
      return it.attributeName.toLowerCase().includes(filterdata);
    });
  }

}
