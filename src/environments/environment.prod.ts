export const environment = {
  production: true,
  // localhost
  // 'menuserviceUrl':"https://localhost:44321/MenuMaster/",

    // server
    'menuserviceUrl':"http://51.141.184.168:8000/MenuMaster/",
    'roleserviceUrl':"https://localhost:44321/MenuMaster/",
    'userGroupserviceUrl':"http://51.141.184.168:8000/UserGroup/",
    'adminUserBoardserviceUrl':"http://51.141.184.168:8000/AdminUserBoard/",
  'clientAdministrationServiceUrl':"http://51.141.184.168:8000/ClientDetails/",
  'userManagementServiceUrl':"http://51.141.184.168:8000/UserManagement/",
  'servicecatalogUrl':"http://51.141.184.168:8000/ServiceCatalog/"
};
